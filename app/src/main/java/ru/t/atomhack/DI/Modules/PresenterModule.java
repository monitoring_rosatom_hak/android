package ru.t.atomhack.DI.Modules;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.DI.PerActivity;
import ru.t.atomhack.Presenters.Interfaces.ILoginPresenter;
import ru.t.atomhack.Presenters.Interfaces.IPresenter;
import ru.t.atomhack.Presenters.Interfaces.ISlidingMenuPresenter;
import ru.t.atomhack.Presenters.LoginPresenter;
import ru.t.atomhack.Presenters.Presenter;
import ru.t.atomhack.Presenters.SlidingMenuPresenter;

@Module
public class PresenterModule {
    //@Singleton
    @PerActivity
    @Provides
    ILoginPresenter providePresenterLogin() {
        return new LoginPresenter();
    }

    @PerActivity
    @Provides
    IPresenter providePresenter() {
        return new Presenter();
    }

    @Provides
    public ISlidingMenuPresenter provideSlidingMenuPresenter() { return new SlidingMenuPresenter(); }

}
