package ru.t.atomhack.DI.Components;


import android.media.audiofx.Equalizer;

import javax.inject.Singleton;

import dagger.Component;
import ru.t.atomhack.App;
import ru.t.atomhack.DI.Modules.AppModule;
import ru.t.atomhack.DI.Modules.AuthModelModule;
import ru.t.atomhack.DI.Modules.CommercialModelModule;
import ru.t.atomhack.DI.Modules.MapModelModule;
import ru.t.atomhack.DI.Modules.RequestModelModule;
import ru.t.atomhack.DI.Modules.SettingsModelModule;
import ru.t.atomhack.DI.Modules.SubscriptionModule;
import ru.t.atomhack.DI.PerActivity;
import ru.t.atomhack.Models.AuthModelImpl;
import ru.t.atomhack.Models.CommercialModelImpl;
import ru.t.atomhack.Models.MapModel;
import ru.t.atomhack.Models.MapModelImpl;
import ru.t.atomhack.Models.RequestModel;
import ru.t.atomhack.Models.RequestModelImpl;
import ru.t.atomhack.Models.SettingsModelImpl;
import ru.t.atomhack.Presenters.BasePresenter;
import ru.t.atomhack.Presenters.CommercialPresenter;
import ru.t.atomhack.Presenters.MapPresenter;
import ru.t.atomhack.Presenters.Presenter;
import ru.t.atomhack.Presenters.RequestPresenter;
import ru.t.atomhack.Presenters.SettingsPresenter;

@Singleton
@Component(modules={
        AppModule.class,
        AuthModelModule.class,
        CommercialModelModule.class,
        RequestModelModule.class,
        MapModelModule.class,
        SettingsModelModule.class,
        SubscriptionModule.class
}, dependencies = {}
)

public interface AppComponent {
    void inject(App app);
    void inject(AuthModelImpl authModel);
    void inject(Presenter presenter);
    void inject(BasePresenter basePresenter);
    void inject(CommercialPresenter commercialPresenter);
    void inject(RequestPresenter requestPresenter);
    void inject(MapPresenter mapPresenter);
    void inject(SettingsPresenter settingsPresenter);
    void inject(RequestModelImpl requestModel);
    void inject(CommercialModelImpl commercialModel);
    void inject(MapModelImpl mapModel);
    void inject(SettingsModelImpl settingsModel);
}