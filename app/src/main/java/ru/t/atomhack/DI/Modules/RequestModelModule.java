package ru.t.atomhack.DI.Modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.Models.Api.RequestApi;
import ru.t.atomhack.Models.Api.RequestApiModule;
import ru.t.atomhack.Models.ConstClass;
import ru.t.atomhack.Models.RequestModel;
import ru.t.atomhack.Models.RequestModelImpl;

@Module
public class RequestModelModule {
    @Provides
    @Singleton
    public RequestModel provideDataRepository() {
        return new RequestModelImpl();
    }

    @Provides
    @Singleton
    RequestApi provideRequestApi() {
        return RequestApiModule.getRestApiInterface(ConstClass.BASE_URL);
    }

}
