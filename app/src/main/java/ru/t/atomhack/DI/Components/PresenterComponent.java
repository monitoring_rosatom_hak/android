package ru.t.atomhack.DI.Components;

import dagger.Component;

import ru.t.atomhack.DI.Modules.PresenterModule;
import ru.t.atomhack.DI.PerActivity;
import ru.t.atomhack.Presenters.Interfaces.ILoginPresenter;
import ru.t.atomhack.Presenters.Interfaces.IPresenter;
import ru.t.atomhack.Presenters.Interfaces.ISlidingMenuPresenter;

@PerActivity
@Component(
        modules = {
                PresenterModule.class
        },
        dependencies = {
                AppComponent.class
        }
)

public interface PresenterComponent {
    ILoginPresenter getLoginPresenter();
    IPresenter getPresenter();
    ISlidingMenuPresenter getSlidingMenuPresenter();
}
