package ru.t.atomhack.DI.Modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.Models.Api.AuthApi;
import ru.t.atomhack.Models.Api.AuthApiModule;
import ru.t.atomhack.Models.Api.CommercialApi;
import ru.t.atomhack.Models.Api.CommercialApiModule;
import ru.t.atomhack.Models.AuthModel;
import ru.t.atomhack.Models.AuthModelImpl;
import ru.t.atomhack.Models.CommercialModel;
import ru.t.atomhack.Models.CommercialModelImpl;
import ru.t.atomhack.Models.ConstClass;

@Module
public class AuthModelModule {
    @Provides
    @Singleton
    public AuthModel provideDataRepository() {
        return new AuthModelImpl();
    }
    
    @Provides
    @Singleton
    AuthApi provideAuthApi() {
        return AuthApiModule.getAuthApiInterface(ConstClass.BASE_URL);
    }

}
