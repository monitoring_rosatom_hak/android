package ru.t.atomhack.DI.Modules;

import android.app.Activity;

import androidx.appcompat.app.AppCompatActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.Presenters.Interfaces.ISlidingMenuPresenter;
import ru.t.atomhack.Views.Activities.SlidingMenu;
import ru.t.atomhack.Views.ISlidingMenu;


@Module
public class ActivityModule {
    private final AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    Activity activity() {
        return this.activity;
    }

    @Provides
    @Singleton
    public ISlidingMenu provideRepository() {return new SlidingMenu();}
}
