package ru.t.atomhack.DI.Modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.Models.Api.MapApi;
import ru.t.atomhack.Models.Api.MapApiModule;
import ru.t.atomhack.Models.Api.SettingsApi;
import ru.t.atomhack.Models.Api.SettingsApiModule;
import ru.t.atomhack.Models.ConstClass;
import ru.t.atomhack.Models.MapModel;
import ru.t.atomhack.Models.MapModelImpl;
import ru.t.atomhack.Models.SettingsModel;
import ru.t.atomhack.Models.SettingsModelImpl;

@Module
public class SettingsModelModule {
    @Provides
    @Singleton
    public SettingsModel provideDataRepository() {
        return new SettingsModelImpl();
    }

    @Provides
    @Singleton
    SettingsApi provideRequestApi() {
        return SettingsApiModule.getRestApiInterface(ConstClass.BASE_URL);
    }

}
