package ru.t.atomhack.DI.Modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.App;

@Module
public class AppModule{
    private Application app;
    public AppModule(App app){
        this.app = app;
    }

    @Provides
    @Singleton
    Context provideContext(){return app;}
}