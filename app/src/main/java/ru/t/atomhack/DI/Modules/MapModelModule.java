package ru.t.atomhack.DI.Modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.Models.Api.MapApi;
import ru.t.atomhack.Models.Api.MapApiModule;
import ru.t.atomhack.Models.ConstClass;
import ru.t.atomhack.Models.MapModel;
import ru.t.atomhack.Models.MapModelImpl;

@Module
public class MapModelModule {
    @Provides
    @Singleton
    public MapModel provideDataRepository() {
        return new MapModelImpl();
    }

    @Provides
    @Singleton
    MapApi provideRequestApi() {
        return MapApiModule.getRestApiInterface(ConstClass.BASE_URL);
    }

}
