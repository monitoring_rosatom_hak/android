package ru.t.atomhack.DI.Modules;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.t.atomhack.Models.ConstClass;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;


@Module
public class SubscriptionModule {

    @Provides
    @Singleton
    @Named(ConstClass.UI_THREAD)
    Scheduler provideSchedulerUI() {
        return AndroidSchedulers.mainThread();
    }


    @Provides
    @Singleton
    @Named(ConstClass.IO_THREAD)
    Scheduler provideSchedulerIO() {
        return Schedulers.io();
    }


    @Provides
    public CompositeSubscription provideCompositeSubscription(){
        return new CompositeSubscription();
    }
}
