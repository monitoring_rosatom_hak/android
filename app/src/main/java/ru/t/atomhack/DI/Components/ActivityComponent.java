package ru.t.atomhack.DI.Components;

import javax.inject.Singleton;

import dagger.Component;
import ru.t.atomhack.DI.Modules.ActivityModule;
import ru.t.atomhack.DI.PerActivity;
import ru.t.atomhack.Views.ISlidingMenu;

@Singleton
@Component(
        modules = {
                ActivityModule.class
        }
)

public interface ActivityComponent {
        ISlidingMenu getSlidingMenuView();
}
