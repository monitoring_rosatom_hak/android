package ru.t.atomhack.Presenters;



import ru.t.atomhack.Presenters.Interfaces.ISlidingMenuPresenter;
import ru.t.atomhack.Views.ISlidingMenu;

public class SlidingMenuPresenter extends BaseViewPresenter implements ISlidingMenuPresenter {

    @Override
    public void setView(ISlidingMenu mView) {
        this.mView = mView;
        super.setView(mView);
    }

    private ISlidingMenu mView;
}

