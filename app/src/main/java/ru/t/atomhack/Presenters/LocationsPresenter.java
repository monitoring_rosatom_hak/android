package ru.t.atomhack.Presenters;


import java.util.ArrayList;

import javax.inject.Inject;

import ru.t.atomhack.App;
import ru.t.atomhack.Models.CommercialModel;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.NullClass;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SaveOrderParams;
import ru.t.atomhack.Models.PARAM.ServiceOwnerParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.ICommercialView;
import ru.t.atomhack.Views.IMain;
import rx.Observer;
import rx.Subscription;

public class LocationsPresenter extends BaseViewPresenter {

    private ICommercialView mView;

    public void setParentView(IMain parentView) {
        this.parentView = parentView;
    }

    public void setView(ICommercialView view) {
        super.setView(view);
        mView = view;
        showFabBtn();
    }

    private IMain parentView;

    private String owner;
    private String servCode;
    private IdNameDescrItem tariff;
    private String description;

    public void setTariff(IdNameDescrItem tariff) {
        this.tariff = tariff;
    }

    public void setDescription(String description) {
        this.description = description;
        showFabBtn();
    }

    public void setOwner(String owner) {
        this.owner = owner;
        showFabBtn();
    }

    public void setServCode(String servCode) {
        this.servCode = servCode;
        showFabBtn();
    }

    private void showFabBtn() {
        if (parentView != null)
            parentView.FabVisible(true);
    }

    @Inject
    protected CommercialModel commercialModel;

    public LocationsPresenter(IBaseView locationFragment) {
        super();
        App.getComponent().inject(this);
        setView(locationFragment);
    }


    public void LoadServiceList(IOnCallBackListener callBackListener) {
        showLoading(true);
        SessionParams params = new SessionParams(authModel.getSessionId());
        Subscription subscription = commercialModel.GetServices(params)
                .subscribe(new Observer<ArrayList<CodeNameItem>>() {
                    ArrayList<CodeNameItem> itemList = new ArrayList<>();

                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<CodeNameItem> result) {
                        if (result != null) {
//                            itemList = result;
                            itemList.add(new CodeNameItem("ctv", "Кабельное ТВ"));
                            itemList.add(new CodeNameItem("internet", "Интернет"));
                            itemList.add(new CodeNameItem("smart", "Умный дом"));
                            itemList.add(new CodeNameItem("security", "Охрана"));
                        }
                    }
                });
        addSubscription(subscription);

    }

    public void LoadOwnerList(IOnCallBackListener callBackListener) {
        if (servCode == null) {
            view.showError("Выберите вначале услугу");
            return;
        }
        showLoading(true);
        CodeParams params = new CodeParams(servCode, authModel.getSessionId());
        Subscription subscription = commercialModel.GetOwners(params)
                .subscribe(new Observer<ArrayList<CodeNameItem>>() {
                    ArrayList<CodeNameItem> itemList = new ArrayList<>();
                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<CodeNameItem> result) {
                        if (result != null) {
//                            itemList = result;
                            itemList.add(new CodeNameItem("o1", "Поставщик 1"));
                            itemList.add(new CodeNameItem("o2", "Поставщик 2"));
                            itemList.add(new CodeNameItem("o3", "Поставщик 3"));
                            itemList.add(new CodeNameItem("o4", "Поставщик 4"));
                        }
                    }
                });
        addSubscription(subscription);

    }

    public void LoadTariffList(IOnCallBackListener callBackListener) {
        if (servCode == null || owner == null) {
            view.showError("Выберите вначале услугу и постевщика");
            return;
        }
        showLoading(true);
        ServiceOwnerParams params = new ServiceOwnerParams(servCode, owner, authModel.getSessionId());
        Subscription subscription = commercialModel.GetTariffs(params)
                .subscribe(new Observer<ArrayList<IdNameDescrItem>>() {
                    ArrayList<IdNameDescrItem> itemList = new ArrayList<>();
                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<IdNameDescrItem> result) {
                        if (result != null) {
//                            itemList = result;
                            itemList.add(new IdNameDescrItem(1, "Тариф 1", "Описание тарифа 1"));
                            itemList.add(new IdNameDescrItem(2, "Тариф 2", "Описание тарифа 2"));
                            itemList.add(new IdNameDescrItem(3, "Тариф 3", "Описание тарифа 3"));
                        }
                    }
                });
        addSubscription(subscription);
    }

    public void saveOrder() {
        if (owner == null || tariff == null || servCode == null || description == null){
            view.showError("Не все параметры указаны");
            return;
        }
        showLoading(true);
        SaveOrderParams params = new SaveOrderParams(servCode, owner, tariff.getId(), description, authModel.getSessionId());
        Subscription subscription = commercialModel.SaveOrder(params)
                .subscribe(new Observer<NullClass>() {
                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        mView.done();
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(NullClass result) {
                        //
                    }
                });
        addSubscription(subscription);
        servCode = null;
        owner = null;
        description = null;
    }
}

