package ru.t.atomhack.Presenters;

import android.content.Context;

import javax.inject.Inject;

import ru.t.atomhack.App;
import ru.t.atomhack.Models.AuthModel;
import ru.t.atomhack.Presenters.Interfaces.IBasePresenter;
import ru.t.atomhack.Utils.IOnCallBackListener;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

public abstract class BasePresenter implements IBasePresenter {

    protected IOnCallBackListener onCallBackListener;

    @Inject
    protected Context _ctx;

    @Inject
    protected AuthModel authModel;

    @Inject
    protected CompositeSubscription compositeSubscription;


    public BasePresenter(){
        App.getComponent().inject(this);
    }

    @Override
    public void setOnCallBackListener(IOnCallBackListener onCallBackListener) {
        this.onCallBackListener = onCallBackListener;
    }
    protected void addSubscription(Subscription subscription){
        compositeSubscription.add(subscription);
    }

    public void onStop() {
        compositeSubscription.clear();
    }


}
