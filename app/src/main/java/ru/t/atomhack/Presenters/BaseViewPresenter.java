package ru.t.atomhack.Presenters;


import java.io.IOException;

import ru.t.atomhack.Presenters.Interfaces.IBaseViewPresenter;
import ru.t.atomhack.Views.IBaseView;


public abstract class BaseViewPresenter extends BasePresenter implements IBaseViewPresenter {

    public void setView(IBaseView view) {
        this.view = view;
    }

    protected IBaseView view;


    protected void showLoading(boolean visiblity){
        if (view!=null)
            view.showLoading(visiblity);
    }

    protected void showError(Object e){
        if (view!=null){
            String message = e.toString();

            if (e instanceof Throwable)
                message = ((Throwable)e).getMessage();

            view.showError(message);
        }

    }

}
