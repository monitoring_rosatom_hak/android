package ru.t.atomhack.Presenters;


import java.util.ArrayList;

import javax.inject.Inject;

import ru.t.atomhack.App;
import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.Models.DTO.PointItem;
import ru.t.atomhack.Models.NullClass;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SaveSettingsParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import ru.t.atomhack.Models.SettingsModel;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IMain;
import rx.Observer;
import rx.Subscription;

public class SettingsPresenter extends BaseViewPresenter {

    public void setParentView(IMain parentView) {
        this.parentView = parentView;
        showFabBtn();
    }

    public void setView(IBaseView view) {
        super.setView(view);
        showFabBtn();
    }

    private IMain parentView;

    private void showFabBtn() {
        if (parentView != null)
            parentView.FabVisible(true);
    }

    @Inject
    protected SettingsModel settingsModel;

    public SettingsPresenter(IBaseView baseView) {
        App.getComponent().inject(this);
        setView(baseView);
        showFabBtn();
    }

    public void LoadCityNotifyList(IOnCallBackListener callBackListener) {
        showLoading(true);
        CodeParams params = new CodeParams("city", authModel.getSessionId());
        Subscription subscription = settingsModel.GetSettings(params)
                .subscribe(new Observer<ArrayList<IdNameCheckItem>>() {
                    ArrayList<IdNameCheckItem> itemList = new ArrayList<>();

                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<IdNameCheckItem> result) {
                        if (result != null) {
//                            itemList = result;
                            itemList.add(new IdNameCheckItem(1, "Пожары", true));
                            itemList.add(new IdNameCheckItem(2, "Радиацонный фон", false));
                        }
                    }
                });
        addSubscription(subscription);
    }

    public void LoadLocalNotifyList(String locationCode, IOnCallBackListener callBackListener) {
        showLoading(true);
        CodeParams params = new CodeParams(locationCode, authModel.getSessionId());
        Subscription subscription = settingsModel.GetSettings(params)
                .subscribe(new Observer<ArrayList<IdNameCheckItem>>() {
                    ArrayList<IdNameCheckItem> itemList = new ArrayList<>();

                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<IdNameCheckItem> result) {
                        if (result != null) {
//                            itemList = result;
                            itemList.add(new IdNameCheckItem(1001, "Кража в доме", false));
                            itemList.add(new IdNameCheckItem(1002, "Разломанный ящик", true));
                            itemList.add(new IdNameCheckItem(1003, "Разбито окно в подъезде", false));
                        }
                    }
                });
        addSubscription(subscription);
    }

    public void saveNotifySettings(ArrayList<IdNameCheckItem> list) {
        showLoading(true);
        SaveSettingsParams params = new SaveSettingsParams(list, authModel.getSessionId());
        Subscription subscription = settingsModel.SaveSettings(params)
                .subscribe(new Observer<NullClass>() {
                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        showError("Настройки сохранены");
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(NullClass result) {
                        //
                    }
                });
        addSubscription(subscription);
    }
}

