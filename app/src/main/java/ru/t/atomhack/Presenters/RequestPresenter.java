package ru.t.atomhack.Presenters;


import java.util.ArrayList;

import javax.inject.Inject;

import ru.t.atomhack.App;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.DTO.Request;
import ru.t.atomhack.Models.NullClass;
import ru.t.atomhack.Models.PARAM.SaveRequestParams;
import ru.t.atomhack.Models.PARAM.IdParams;
import ru.t.atomhack.Models.PARAM.ServiceOwnerParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import ru.t.atomhack.Models.RequestModel;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IFullPhoto;
import ru.t.atomhack.Views.IMain;
import ru.t.atomhack.Views.IRequestView;
import rx.Observer;
import rx.Subscription;

public class RequestPresenter extends BaseViewPresenter {
    public void setParentView(IMain parentView) {
        this.parentView = parentView;
    }

    private IMain parentView;

    public void setType(CodeNameItem type) {
        this.type = type;
        showFabBtn();
    }

    private CodeNameItem type;
    private IRequestView mView;

    public RequestPresenter(IRequestView view) {
        App.getComponent().inject(this);
        super.setView(view);
        this.mView = view;
    }


    public Request getRequest() {
        return _request;
    }

    public void setRequest(Request request) {
        if (type != null)
            request.setType(type.getCode());
        if (description != null)
            request.setDescription(description);
        this._request = request;
        showFabBtn();
    }

    private Request _request;

    public void setDescription(String description) {
        this.description = description;
        showFabBtn();
    }

    private void showFabBtn() {
        if (parentView != null)
            parentView.FabVisible(_request != null && type != null && description != null);
    }

    private String description;

    @Inject
    protected RequestModel requestModel;

    public void LoadTypeList(IOnCallBackListener callBackListener) {
        showLoading(true);
        SessionParams params = new SessionParams(authModel.getSessionId());
        Subscription subscription = requestModel.GetTypes(params)
                .subscribe(new Observer<ArrayList<CodeNameItem>>() {
                    ArrayList<CodeNameItem> itemList = new ArrayList<>();

                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<CodeNameItem> result) {
                        if (result != null) {
                            itemList = result;
//                            itemList.add(new CodeNameItem("hole", "Яма на дороге"));
//                            itemList.add(new CodeNameItem("garbage", "Мусор"));
//                            itemList.add(new CodeNameItem("entrance", "Грязь в подъезде"));
//                            itemList.add(new CodeNameItem("improvement", "Благоустройство"));
                        }
                    }
                });
        addSubscription(subscription);
    }

    public void loadRequest(int id) {
        showLoading(true);
        IdParams param = new IdParams(id, authModel.getSessionId());
        Subscription subscription = requestModel.GetRequest(param).subscribe(new Observer<Request>() {
            @Override
            public void onCompleted() {
                showLoading(false);
            }

            @Override
            public void onError(Throwable e) {
                showLoading(false);
                showError(e);
            }

            @Override
            public void onNext(Request result) {
                if (result != null) {
                    _request.init(result);
                }
            }
        });
        addSubscription(subscription);
    }

    public void delPhoto(int id) {
        showLoading(true);
        if (_request != null && _request.getId() == id) {
            _request.clearImg();
            SaveRequestParams params = new SaveRequestParams(id, _request.getType(), _request.getDescription(), null, authModel.getSessionId());
            Subscription subscription = requestModel.SaveRequest(params)
                    .subscribe(new Observer<NullClass>() {
                        @Override
                        public void onCompleted() {
                            showLoading(false);
                            mView.clearImg();
                        }

                        @Override
                        public void onError(Throwable e) {
                            showError(e);
                            showLoading(false);
                        }

                        @Override
                        public void onNext(NullClass obj) {
                            if (obj == null) {
                                showError("unknown error");
                            }
                        }
                    });
            addSubscription(subscription);
        }else
            mView.clearImg();


    }

    public void saveRequest() {
        if (_request == null || type == null || description == null){
            view.showError("Не все параметры указаны");
            return;
        }
        showLoading(true);
        SaveRequestParams params = new SaveRequestParams(null, type.getCode(), description, _request.getImg(), authModel.getSessionId());
        Subscription subscription = requestModel.SaveRequest(params)
                .subscribe(new Observer<NullClass>() {
                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        mView.done();
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(NullClass nullClass) {
                        //
                    }
                });
        addSubscription(subscription);
    }

}

