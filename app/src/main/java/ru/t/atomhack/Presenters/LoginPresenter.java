package ru.t.atomhack.Presenters;


import android.text.TextUtils;

import ru.t.atomhack.Presenters.Interfaces.ILoginPresenter;
import ru.t.atomhack.Views.ILogin;


public class LoginPresenter extends BaseViewPresenter implements ILoginPresenter {
    private ILogin mView;

    @Override
    public void setView(ILogin mView) {
        super.setView(mView);
        this.mView = mView;
    }


    private boolean isPasswordValid(String password) {
        return password != null && password.length() >= 4;
    }

    @Override
    public void attemptLogin() {

        mView.showLoginError(null);
        mView.showPasswordError(null);

        // Store values at the time of the login attempt.
        String login = mView.getUserName();
        String password = mView.getPassword();

        boolean cancel = false;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mView.showPasswordError("Пароль слишком короткий");
            cancel = true;
        }

        if (TextUtils.isEmpty(login)) {
            mView.showLoginError("Необходимо заполнить");
            cancel = true;
        }

        if (TextUtils.isEmpty(password)) {
            mView.showPasswordError("Необходимо заполнить");
            cancel = true;
        }
        if (!cancel) {
            showLoading(true);
            //"pass" = password;
            //"user" = login;
            if (onCallBackListener != null)
                onCallBackListener.callBack(true);

        }
    }
}

