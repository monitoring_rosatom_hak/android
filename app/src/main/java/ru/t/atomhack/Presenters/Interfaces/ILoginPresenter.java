package ru.t.atomhack.Presenters.Interfaces;


import ru.t.atomhack.Views.ILogin;

public interface ILoginPresenter extends IBaseViewPresenter{
    void setView(ILogin mView);
    void attemptLogin();
}
