package ru.t.atomhack.Presenters.Interfaces;

import ru.t.atomhack.Views.IBaseView;

public interface IBaseViewPresenter extends IBasePresenter {
    void setView(IBaseView mView);
}
