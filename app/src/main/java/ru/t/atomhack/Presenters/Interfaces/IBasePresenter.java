package ru.t.atomhack.Presenters.Interfaces;

import ru.t.atomhack.Utils.IOnCallBackListener;

public interface IBasePresenter {
    void setOnCallBackListener(IOnCallBackListener onCallBackListener);
}
