package ru.t.atomhack.Presenters;


import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.inject.Inject;

import ru.t.atomhack.Presenters.Interfaces.IPresenter;

import static ru.t.atomhack.App.getComponent;


public class Presenter extends BaseViewPresenter implements IPresenter {


    public Presenter() {
        getComponent().inject(this);
    }
}

