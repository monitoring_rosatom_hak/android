package ru.t.atomhack.Presenters;


import java.util.ArrayList;

import javax.inject.Inject;

import ru.t.atomhack.App;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.Models.DTO.PointItem;
import ru.t.atomhack.Models.MapModel;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IMain;
import rx.Observer;
import rx.Subscription;

public class MapPresenter extends BaseViewPresenter {

    public void setParentView(IMain parentView) {
        this.parentView = parentView;
    }

    private IMain parentView;


    @Inject
    protected MapModel mapModel;

    public MapPresenter(IBaseView baseView) {
        App.getComponent().inject(this);
        setView(baseView);
    }

    public void LoadEventList(String type, IOnCallBackListener callBackListener) {
        showLoading(true);
        CodeParams params = new CodeParams(type, authModel.getSessionId());
        Subscription subscription = mapModel.GetEventList(params)
                .subscribe(new Observer<ArrayList<PointItem>>() {
                    ArrayList<PointItem> itemList = new ArrayList<>();
                    @Override
                    public void onCompleted() {
                        showLoading(false);
                        if (callBackListener != null)
                            callBackListener.callBack(itemList);
                    }

                    @Override
                    public void onError(Throwable e) {
                        showError(e);
                        showLoading(false);
                    }

                    @Override
                    public void onNext(ArrayList<PointItem> result) {
                        if (result != null) {
                            itemList = result;
                            /*switch (type){
                                case "radiation" :
                                    itemList.add(new PointItem(1, "8.9", type, "ПК Парусинка\n" +
                                            "мощность экспозиционной дозы (мкР/ч)\n" +
                                            "\n" +
                                            "Значение: 9.3 мкР/ч\n" +
                                            "\n" +
                                            "Время измерения: 2020-05-29 03:31:40\n" +
                                            "Максимально допустимое значение для Томской области - 30 мкР/ч\n",56.5836,84.9104));
                                    itemList.add(new PointItem(2, "9.3", type, "ПК ЕДДС\n" +
                                            "мощность экспозиционной дозы (мкР/ч)\n" +
                                            "\n" +
                                            "Значение: 9.3 мкР/ч\n" +
                                            "\n" +
                                            "Время измерения: 2020-05-30 20:07:18\n" +
                                            "Максимально допустимое значение для Томской области - 30 мкР/ч\n"
                                            ,56.58679, 84.93617));
                                    break;
                                case "fire" :
                                    itemList.add(new PointItem(1, "", type, "горим",56.58934, 84.92727));
                                    itemList.add(new PointItem(2, "", type, "горим",56.5579, 84.93617));
                                    break;
                            }*/
                        }
                    }
                });
        addSubscription(subscription);
    }
}

