package ru.t.atomhack.Views;

public interface IRequestView extends IBaseView {
    void done();
    void clearImg();
}
