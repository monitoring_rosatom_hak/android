package ru.t.atomhack.Views.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import ru.t.atomhack.Presenters.Interfaces.ILoginPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Views.ILogin;

import static ru.t.atomhack.Utils.GlobalUtils.showProgress;


public class Login extends CompatActivity implements ILogin {


    ILoginPresenter mPresenter;

    @BindView(R.id.login)
    AutoCompleteTextView mLoginView;
    @BindView(R.id.password)
    EditText mPasswordView;
    @BindView(R.id.checkBox)
    CheckBox mCheckBox;
    @BindView(R.id.login_progress)
    View mProgressView;
    @BindView(R.id.login_form)
    View mLoginFormView;

    @OnClick(R.id.login_form)
    void OnClickLoginForm(){
        mPasswordView.setError(null);
        mLoginView.setError(null);
    }

    @OnClick(R.id.action_sign_in)
    public void onLoginClick(View view) {
        mPresenter.attemptLogin();
    }

    @OnEditorAction(R.id.password)
    boolean OnPasswordAction(TextView textView, int id, KeyEvent keyEvent){
        if (id == R.id.login || id == EditorInfo.IME_NULL) {
            mPresenter.attemptLogin();
            return true;
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mPresenter = mPresenterComponent.getLoginPresenter();

        SharedPreferences settings = getSharedPreferences("PrefLogin", Context.MODE_PRIVATE);
        String pUserName = settings.getString("UserName", "UserName");
        String pPassword = settings.getString("Password", "Password");
        mPresenter.setView(this);

        mPresenter.setOnCallBackListener(o -> {
            SharedPreferences settingList = getSharedPreferences("PrefLogin", Context.MODE_PRIVATE);
            SharedPreferences.Editor edCL = settingList.edit();
            if (mCheckBox.isChecked()){
                edCL.putString("Password",getPassword());
            }else{
                edCL.clear();
            }
            edCL.putString("UserName",getUserName());
            edCL.apply();


            Intent startIntent = new Intent(Login.this, Main.class);

            startActivity(startIntent);
            //finish();
            return true;
        });

        if (!pUserName.isEmpty()) {
            mLoginView.setText(pUserName);
        }
        if (!pPassword.isEmpty()) {
            mPasswordView.setText(pPassword);
            mCheckBox.setChecked(true);
            //mPresenter.attemptLogin();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        showLoading(false);
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    /*
    @Override
    public void showError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

    public void showLoading(boolean isLoading) {
        //showProgress(isLoading, mLoginFormView, mProgressView);
    }
    */
    public void showLoginError(String error) {
        mLoginView.setError(error);
        //mLoginView.setHintTextColor(Color.RED);
    }
    public void showPasswordError(String error) {
        mPasswordView.setError(error);
        //mPasswordView.setHintTextColor(Color.RED);
    }
    public String getUserName() {
        return mLoginView.getText().toString();
    }
    public String getPassword() {
        return mPasswordView.getText().toString();
    }

    @Override
    public View GetView() {
        return mLoginFormView;
    }

    @Override
    public void showLoading(boolean isLoading) {
        showProgress(isLoading, mLoginFormView, mProgressView);
    }
}

