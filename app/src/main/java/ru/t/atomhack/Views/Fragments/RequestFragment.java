package ru.t.atomhack.Views.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;

import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.Request;
import ru.t.atomhack.Presenters.RequestPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.Activities.FullPhoto;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IMain;
import ru.t.atomhack.Views.IRequestView;

import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showAlert;
import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showSelectDialog;
import static ru.t.atomhack.Utils.GlobalUtils.showProgress;

public class RequestFragment extends BaseFragment implements IRequestView {


    private String _description;
    private String _reason;
    private RequestPresenter _requestPresenter;
    private Bitmap _bitmap;

    public static RequestFragment newInstance() {
        RequestFragment fragment = new RequestFragment();
        Bundle arguments = new Bundle();
        fragment.setArguments(arguments);
        return fragment;
    }

    private boolean _change;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMain) {
            setParentView((IMain) context);
        }
    }

    @BindView(R.id.photoImg)
    ImageView photoImg;
    @BindView(R.id.progressBar)
    View mProgressView;
    @BindView(R.id.action_layout)
    View mActionLayoutView;
    @BindView(R.id.typeName)
    TextView typeName;
    @BindView(R.id.descriptionName)
    TextView descriptionName;

    @OnClick(R.id.typeRow)
    void onTypeRowClick(View v) {
        _requestPresenter.LoadTypeList(o -> {
            if (o != null && o instanceof ArrayList) {
                ArrayList<CodeNameItem> typeList = (ArrayList<CodeNameItem>) o;
                List<String> list = new ArrayList<>();
                int j = -1;
                int k = -1;
                list.clear();
                for (CodeNameItem r : typeList) {
                    list.add(r.getName());
                    if (r.getName().equals(typeName.getText())) k = j;
                    j++;
                }

                if (getContext() instanceof FragmentActivity)
                    showSelectDialog((FragmentActivity) getContext(), "Выбор типа", list, k, i -> {
                        if (i > -1) {
                            typeName.setText(typeList.get(i).getName());
                            _requestPresenter.setType(typeList.get(i));
                        }

                    });
            }
            return false;
        });
    }

    @OnClick(R.id.descriptionRow)
    void onDescriptionRowClick(View v) {
        View d = getActivity().getLayoutInflater().inflate(R.layout.text_view_dialod, null);
        EditText editTextDescription = d.findViewById(R.id.editTextDescription);
        if (editTextDescription != null) {
            editTextDescription.setText((CharSequence) descriptionName.getText().toString());
        }
        if (getContext() instanceof FragmentActivity)
            showAlert((FragmentActivity) getContext(), "Указание примечания", "", o -> {
                        if (editTextDescription != null) {
                            descriptionName.setText(editTextDescription.getText().toString());
                            _requestPresenter.setDescription(editTextDescription.getText().toString());
                            return true;
                        }
                        return false;
                    }
                    , d);
    }

    public void setBitmap(Bitmap _bitmap) {
        this._bitmap = _bitmap;
        _change = true;
        if (_bitmap != null) {
            photoImg.setImageBitmap(_bitmap);
            Request request = _requestPresenter.getRequest();
            if (request == null)
                request = new Request(_bitmap);
            else
                request.setImg(_bitmap);
            _requestPresenter.setRequest(request);
        } else
            photoImg.setImageResource(R.drawable.ic_image_grey);
    }

    @OnClick(R.id.photoImg)
    void onImgClick(View v) {
        if (_bitmap != null) {
            Intent startIntent = new Intent(getParentView().getContext(), FullPhoto.class);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            if (_bitmap != null)
                _bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
            byte[] bytes = stream.toByteArray();
            try {
                stream.flush();
                stream.close();
            } catch (IOException e) {
                //e.printStackTrace();
            }
            long id = 1;
            startIntent.putExtra("Image", bytes);
            startIntent.putExtra("ImgId", id);
            startActivityForResult(startIntent, 1);
        } else {
            PickSetup setup = new PickSetup();
            PickImageDialog.build(setup).setOnPickResult(r -> {
                setBitmap(r.getBitmap());
            }).show(getFragmentManager());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        //in fragment class callback
        if (resultCode == 1) {
            setBitmap(null);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        _requestPresenter = new RequestPresenter(this);
        _requestPresenter.setParentView(getParentView());
        getParentView().setFabClickListener(o -> {
            _change = false;
            _requestPresenter.saveRequest();
            return false;
        });
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_order, container, false);
        ButterKnife.bind(this, mView);
        mPresenter.setView(this);
        setTitle("Сообщить о проблеме");
        setBitmap(_bitmap);
        return mView;
    }

    @Override
    public void onDestroyView() {
        if (_change)
            _requestPresenter.saveRequest();
        super.onDestroyView();
    }

    @Override
    public View GetView() {
        return getParentView().GetView();
    }

    @Override
    public void showError(Object e) {
        getParentView().showError(e);
    }

    @Override
    public void showLoading(boolean isLoading) {
        showProgress(isLoading, mActionLayoutView, mProgressView);
    }

    @Override
    public void done() {
        _change = false;
        typeName.setText("");
        descriptionName.setText("");
        setBitmap(null);
        showError("Сообщение отправлено");
        getParentView().FabVisible(false);

    }

    @Override
    public void clearImg() {
        setBitmap(null);
    }
}
