package ru.t.atomhack.Views.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.t.atomhack.Models.ActionItem;
import ru.t.atomhack.R;
import ru.t.atomhack.Views.Adapters.ActionListRecyclerViewAdapter;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IMain;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActionsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ActionsListFragment extends BaseFragment implements IBaseView {
    public ActionsListFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.rvList)
    RecyclerView actionList;
    private List<ActionItem> actionItemList;

    public static ActionsListFragment newInstance() {
        ActionsListFragment fragment = new ActionsListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionItemList = new ArrayList<>();
        actionItemList.add(new ActionItem("ПЛАТНЫЕ УСЛУГИ", CommercialFragment.newInstance()));
        actionItemList.add(new ActionItem("К ПОРЯДКУ", RequestFragment.newInstance()));
        actionItemList.add(new ActionItem("КАРТА СОБЫТИЙ", MapFragment.newInstance()));
        actionItemList.add(new ActionItem("НАСТРОЙКА УВЕДОМЛЕНИЙ", SettingsFragment.newInstance()));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, mView);
        mPresenter.setView(this);

        actionList.setAdapter(new ActionListRecyclerViewAdapter(actionItemList, o -> {
                    if (o instanceof ActionItem) {
                        getParentView().replaceFragment(((ActionItem) o).getBaseFragment());
                        return true;
                    }
                    return false;
                })
        );
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMain) {
            setParentView((IMain) context);
        }
    }
}