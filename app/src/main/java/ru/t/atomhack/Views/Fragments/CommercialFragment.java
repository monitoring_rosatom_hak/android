package ru.t.atomhack.Views.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.t.atomhack.Models.RowItem;
import ru.t.atomhack.Presenters.CommercialPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Views.Adapters.RowListRecyclerViewAdapter;
import ru.t.atomhack.Views.ICommercialView;
import ru.t.atomhack.Views.IMain;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CommercialFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CommercialFragment extends BaseFragment implements ICommercialView {
    public CommercialFragment() {
        // Required empty public constructor
    }
    private CommercialPresenter _commercialPresenter;

    @BindView(R.id.rvList)
    RecyclerView rvList;
    private List<RowItem> rowItemList;

    public static CommercialFragment newInstance() {
        CommercialFragment fragment = new CommercialFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rowItemList = new ArrayList<>();
        setTitle("Заказ платной услуги");
        _commercialPresenter = new CommercialPresenter(this);
        _commercialPresenter.setParentView((IMain) getParentView());
        getParentView().setFabClickListener(o -> {
            _commercialPresenter.saveOrder();
            return false;
        });
        rowItemList.add(new RowItem("Набор услуг", "service"));
        rowItemList.add(new RowItem("Поставщик", "owner"));
        rowItemList.add(new RowItem("Тариф", "tariff"));
        rowItemList.add(new RowItem("Примечение", "description"));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, mView);
        mPresenter.setView(this);
        _commercialPresenter.setView(this);
        rvList.setAdapter(new RowListRecyclerViewAdapter(rowItemList, _commercialPresenter));
        return mView;
    }

    public void done(){
        ((RowListRecyclerViewAdapter)rvList.getAdapter()).clearRows();
        getParentView().FabVisible(false);
        showError("услуга заказана");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMain) {
            setParentView((IMain) context);
        }
    }
}