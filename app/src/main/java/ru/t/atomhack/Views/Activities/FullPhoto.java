package ru.t.atomhack.Views.Activities;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

import com.github.chrisbanes.photoview.PhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.Models.DTO.Request;
import ru.t.atomhack.Presenters.RequestPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.Dialogs.DialogsManager;
import ru.t.atomhack.Views.IFullPhoto;

import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showConfirmDialog;


public class FullPhoto extends CompatActivity implements IFullPhoto {

    @BindView(R.id.imageView)       PhotoView imageView;
    @BindView(R.id.delete_action)   View deleteAction;
    private int ImgId;
    private int Num;
    boolean CanDelete = false;
    private RequestPresenter requestPresenter;

    @OnClick(R.id.delete_action)
    void onDeleteClick(View v) {
        if (!CanDelete) return;
        showConfirmDialog(this, "Удалить фотографию?", new DialogsManager.Act() {
            @Override
            public void positiveClick() {
                requestPresenter.delPhoto(ImgId);
                //finish();
            }

            @Override
            public void negativeClick() {

            }
        });

    }

    @OnClick(R.id.close_action)
    void onCloseClick(View v) {
        finish();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_photo);
        //slidingMenu.SetActivity(this);
        ButterKnife.bind(this);
        requestPresenter = new RequestPresenter(null);
        byte[] bytes = getIntent().getByteArrayExtra("Image");
        CanDelete = (bytes != null);
        Integer Id = getIntent().getIntExtra("Id", 0);
        if (bytes == null){
            requestPresenter.loadRequest(Id);
            SetImageBitmap(requestPresenter.getRequest());
        } else {
            android.graphics.Bitmap Image = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
            imageView.setImageBitmap(Image);
        }


        deleteAction.setVisibility(CanDelete ? View.VISIBLE : View.GONE);
    }


    @Override
    public void SetImageBitmap(Request bitmap) {
        //CanDelete = bitmap.CreatedBy != null && bitmap.CreatedBy.equals(mPresenter.getToken().getUsername());
        deleteAction.setVisibility(CanDelete ? View.VISIBLE : View.GONE);
        imageView.setImageBitmap(bitmap.getBitmap());
    }

    @Override
    public View GetView() {
        return imageView;
    }

    @Override
    public void showLoading(boolean isLoading) {

    }
}
