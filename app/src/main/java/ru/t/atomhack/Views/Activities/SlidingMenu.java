package ru.t.atomhack.Views.Activities;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.drawerlayout.widget.DrawerLayout;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import ru.t.atomhack.Presenters.Interfaces.ISlidingMenuPresenter;
import ru.t.atomhack.Presenters.SlidingMenuPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.Dialogs.DialogsManager;
import ru.t.atomhack.Views.Fragments.CommercialFragment;
import ru.t.atomhack.Views.Fragments.LocationFragment;
import ru.t.atomhack.Views.Fragments.MapFragment;
import ru.t.atomhack.Views.Fragments.RequestFragment;
import ru.t.atomhack.Views.Fragments.SettingsFragment;
import ru.t.atomhack.Views.IMain;
import ru.t.atomhack.Views.ISlidingMenu;


import static android.view.Gravity.LEFT;
import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showConfirmDialog;

public class SlidingMenu implements ISlidingMenu {

    ISlidingMenuPresenter mPresenter;

    public DrawerLayout getDrawerLayout() {
        return mDrawerLayout;
    }

    @Nullable
    @BindView(R.id.drawer_layout)
    DrawerLayout mDrawerLayout;
    @Nullable
    @BindView(R.id.user)
    TextView tvUser;
    @Nullable
    @BindView(R.id.programVersion)
    TextView programVersion;

    private Activity activity;

    private IMain parentView;

    public void SetActivity(Activity mActivity) {
        if (mActivity instanceof IMain)
            parentView = (IMain) mActivity;
        activity = mActivity;
        ButterKnife.bind(this, mActivity);
        initMenu();
    }


    @Inject
    public SlidingMenu() {
        this.mPresenter = new SlidingMenuPresenter();

        this.mPresenter.setView(this);

    }

    private void initMenu() {
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                activity, mDrawerLayout, activity.findViewById(R.id.toolbar_actionbar),
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        if (mDrawerLayout != null) {
            mDrawerLayout.addDrawerListener(mDrawerToggle);
            mDrawerToggle.syncState();
        }
    }

    @Optional
    @OnClick(R.id.commercialService)
    void onCommercialServiceClick(View v) {
        parentView.replaceFragment(CommercialFragment.newInstance());
        toogle(true);
    }

    @Optional
    @OnClick(R.id.requestOrder)
    void onRequestOrderClick(View v) {
        parentView.replaceFragment(RequestFragment.newInstance());
        toogle(true);
    }

    @Optional
    @OnClick(R.id.map)
    void onMapClick(View v) {
        parentView.replaceFragment(MapFragment.newInstance());
        toogle(true);
    }

    @Optional
    @OnClick(R.id.notifySettings)
    void onNotifySettingsClick(View v) {
        parentView.replaceFragment(SettingsFragment.newInstance());
        toogle(true);
    }

    @Optional
    @OnClick(R.id.locationList)
    void onLocationListClick(View v) {
        parentView.replaceFragment(LocationFragment.newInstance());
        toogle(true);
    }
    @Optional
    @OnClick(R.id.btnExitDevider)
    void onExitClick(View v) {
        showConfirmDialog(activity,
                "Вы действительно хотите выйти?",
                new DialogsManager.Act() {
                    @Override
                    public void positiveClick() {
                        activity.finish();
                    }

                    @Override
                    public void negativeClick() {
//
                    }
                });

    }

    public void populate() {
        if (tvUser != null) tvUser.setText("тест");
        int sVersionCode = 1;
        String sVersionName = "";
        try {
            PackageInfo pinfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            sVersionCode = pinfo.versionCode;
            sVersionName = pinfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            //
        }
        programVersion.setText(sVersionName + "(" + sVersionCode + ")\n");
    }

    public void toogle(boolean canOpen) {
        if (mDrawerLayout != null)
            if (!mDrawerLayout.isDrawerOpen(LEFT) & canOpen) {
                mDrawerLayout.openDrawer(LEFT);
            } else {
                mDrawerLayout.closeDrawer(LEFT);
            }
    }

    @Override
    public View GetView() {
        return mDrawerLayout;
    }

    @Override
    public void showError(Object e) {

    }

    @Override
    public void showLoading(boolean isLoading) {
        //
    }
}
