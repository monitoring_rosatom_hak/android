package ru.t.atomhack.Views.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ru.t.atomhack.Utils.IOnCallBackListener;


public abstract class AbstractRecyclerViewAdapter<T extends RecyclerView.ViewHolder, TItem> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    protected List<TItem> mValue;
    protected Context _ctx;
    protected IOnCallBackListener callBack;

    public AbstractRecyclerViewAdapter(List<TItem> list, IOnCallBackListener callBack) {
        mValue = list;
        this.callBack = callBack;
    }

    protected abstract View getView(ViewGroup parent);

    protected abstract T initHolder(View view);

    protected abstract void populate(T holder, int position);

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        populate((T)holder,position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _ctx = parent.getContext();
        return initHolder(getView(parent));
    }
    @Override
    public int getItemCount() {
        return mValue!=null?mValue.size():0;
    }

    public void addFirstAll(List<TItem> items) {
        int j=0;
        for (TItem i:items) {
            this.mValue.add(j, i);
            j++;
        }
    }

    public void addAll(List<TItem> list) {
        mValue = list;
        notifyDataSetChanged();
    }
}
