package ru.t.atomhack.Views.Adapters;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

import ru.t.atomhack.Views.Fragments.BaseFragment;


//public class CallPagerAdapter extends FragmentPagerAdapter {
public class PagerAdapter extends FragmentStatePagerAdapter {
    private final List<BaseFragment> mTabList = new ArrayList<>();

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        return mTabList.get(position);
    }

    @Override
    public int getCount() {
        return mTabList.size();
    }

    public void addFrag(BaseFragment fragment) {
        mTabList.add(fragment);
        notifyDataSetChanged();
    }


    @Override
    public CharSequence getPageTitle(int position) {
        return mTabList.get(position).getTitle();
    }

}
