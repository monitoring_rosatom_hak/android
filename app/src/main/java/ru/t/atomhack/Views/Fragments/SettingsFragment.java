package ru.t.atomhack.Views.Fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.Presenters.SettingsPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.Adapters.SettingsListRecyclerViewAdapter;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IMain;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ActionsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SettingsFragment extends BaseFragment implements IBaseView {
    public SettingsFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.rvList1)
    RecyclerView cityNotifyList;
    @BindView(R.id.rvList2)
    RecyclerView localNotifyList;

    private List<IdNameCheckItem> cityNotifyItemList;
    private List<IdNameCheckItem> localNotifyItemList;
    private SettingsPresenter _settingsPresenter;

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Настройка уведомлений");
        _settingsPresenter = new SettingsPresenter(this);
        _settingsPresenter.setParentView((IMain) getParentView());
        getParentView().setFabClickListener(o -> {
            ArrayList<IdNameCheckItem> newList = new ArrayList<>(cityNotifyItemList);
            newList.addAll(localNotifyItemList);
            _settingsPresenter.saveNotifySettings(newList);
            return false;
        });
        _settingsPresenter.LoadCityNotifyList(o -> {
            if (o != null && o instanceof ArrayList) {
                cityNotifyItemList = (List<IdNameCheckItem>) o;
                if (cityNotifyList!=null)
                    ((SettingsListRecyclerViewAdapter)cityNotifyList.getAdapter()).setList(cityNotifyItemList);
                return true;
            }
            return false;
        });
        _settingsPresenter.LoadLocalNotifyList("home",o -> {
            if (o != null && o instanceof ArrayList) {
                localNotifyItemList = (List<IdNameCheckItem>) o;
                if (localNotifyList!=null)
                    ((SettingsListRecyclerViewAdapter)localNotifyList.getAdapter()).setList(localNotifyItemList);
                return true;
            }
            return false;
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, mView);
        mPresenter.setView(this);

        cityNotifyList.setAdapter(new SettingsListRecyclerViewAdapter(cityNotifyItemList));
        localNotifyList.setAdapter(new SettingsListRecyclerViewAdapter(localNotifyItemList));
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMain) {
            setParentView((IMain) context);
        }
    }
}