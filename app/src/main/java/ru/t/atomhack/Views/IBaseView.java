package ru.t.atomhack.Views;

import android.view.View;

public interface IBaseView {
    View GetView();
    void showError(Object e);
    void showLoading(boolean isLoading);
}
