package ru.t.atomhack.Views.Fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.t.atomhack.Models.RowItem;
import ru.t.atomhack.Presenters.CommercialPresenter;
import ru.t.atomhack.Presenters.LocationsPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Views.Adapters.LocationRowListRecyclerViewAdapter;
import ru.t.atomhack.Views.Adapters.RowListRecyclerViewAdapter;
import ru.t.atomhack.Views.ICommercialView;
import ru.t.atomhack.Views.IMain;

import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showAlert;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link LocationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LocationFragment extends BaseFragment implements ICommercialView {
    public LocationFragment() {
        // Required empty public constructor
    }
    private LocationsPresenter locationsPresenter;

    @BindView(R.id.rvList)
    RecyclerView rvList;
    private List<RowItem> rowItemList;

    public static LocationFragment newInstance() {
        LocationFragment fragment = new LocationFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rowItemList = new ArrayList<>();
        setTitle("Список локаций");
        locationsPresenter = new LocationsPresenter(this);
        locationsPresenter.setParentView((IMain) getParentView());
        getParentView().setFabClickListener(o -> {
            if (getContext() != null) {
                View d = ((Activity) getContext()).getLayoutInflater().inflate(R.layout.text_view_dialod, null);
                EditText editTextDescription = d.findViewById(R.id.editTextDescription);
                showAlert(getContext(), "Добавление локации", "", o1 -> {
                            if (editTextDescription != null) {
                                rowItemList.add(new RowItem(editTextDescription.getText().toString(), ""));
                                return true;
                            }
                            return false;
                        }
                        , d);
            }
            return false;
        });
        rowItemList.add(new RowItem("Дом", ""));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, mView);
        mPresenter.setView(this);
        locationsPresenter.setView(this);
        getParentView().SetFabIcon(R.drawable.ic_add_white);
        rvList.setAdapter(new LocationRowListRecyclerViewAdapter(rowItemList, locationsPresenter));
        return mView;
    }

    public void done(){
        ((RowListRecyclerViewAdapter)rvList.getAdapter()).clearRows();
//        getParentView().FabVisible(false);
        showError("Локация добавлена");

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMain) {
            setParentView((IMain) context);
        }
    }
}