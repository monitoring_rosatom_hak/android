package ru.t.atomhack.Views;

import android.app.Activity;

import androidx.drawerlayout.widget.DrawerLayout;


public interface ISlidingMenu extends IBaseView {
    DrawerLayout getDrawerLayout();
    void toogle(boolean canOpen);
    void SetActivity(Activity mActivity);
    void populate();
}
