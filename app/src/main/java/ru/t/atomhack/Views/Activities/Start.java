package ru.t.atomhack.Views.Activities;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.Fragments.ActionsListFragment;
import ru.t.atomhack.Views.Fragments.BaseFragment;
import ru.t.atomhack.Views.IMain;

import static ru.t.atomhack.Utils.GlobalUtils.showProgress;

public class Start extends CompatActivity implements IMain {
    private FragmentManager fragmentManager;
    private static String TAG = "TAG";
    @BindView(R.id.progressBar)
    View mProgressView;
    @BindView(R.id.toolbar_actionbar)
    Toolbar mActionBarToolbar;
    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;
    @BindView(R.id.toolbar_title2)
    TextView toolBarTitle2;
    @BindView(R.id.fab_layout)
    ConstraintLayout fabLayout;
    @BindView(R.id.mntBtn)
    FloatingActionButton mntBtn;
    public TabLayout getMaterialupTabs() {
        return materialupTabs;
    }

    @BindView(R.id.materialupTabs)
    TabLayout materialupTabs;

    @OnClick(R.id.mntBtn)
    void onMntBtnClick(View v) {
        if (fabClickListener != null)
            fabClickListener.callBack(v);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
        mPresenter.setView(this);
        setTitle("Мой Город");
        setSupportActionBar(mActionBarToolbar);
        slidingMenu.SetActivity(this);
        slidingMenu.populate();
        mActionBarToolbar.setNavigationIcon(R.drawable.ic_view_headline);
        fragmentManager = getSupportFragmentManager();
        BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(TAG);
        if (fragment == null) {
            replaceFragment(new ActionsListFragment());
        } else
            fragment.setParentView(this);
    }

    public void setFabClickListener(IOnCallBackListener fabClickListener) {
        this.fabClickListener = fabClickListener;
    }

    private IOnCallBackListener fabClickListener;

    public void replaceFragment(BaseFragment fragment) {
        if (fragmentManager.isStateSaved()) return;
        materialupTabs.setVisibility(View.GONE);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if ((fragment).isAdded()) {
            transaction.remove(fragment);
        }
        //transaction.add(R.id.container, fragment, TAG);
        transaction.replace(R.id.container, fragment, TAG);
        //transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.addToBackStack(null);
        transaction.commit();
        //transaction.commitAllowingStateLoss();
        fragmentManager.executePendingTransactions();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem settings_button = menu.findItem(R.id.notifications);
        settings_button.setOnMenuItemClickListener(menuItem -> {
            int sVersionCode = 1;
            String sVersionName = "";
            try {
                PackageInfo pinfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
                sVersionCode = pinfo.versionCode;
                sVersionName = pinfo.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                //
            }
            Toast.makeText(getContext(), sVersionName + "(" + sVersionCode + ")\n", Toast.LENGTH_LONG).show();
            return true;
        });
        return true;
    }

    @Override
    public void onBackPressed() {
        FabVisible(false);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public View GetView() {
        return mActionBarToolbar;
    }

    @Override
    public void showLoading(boolean isLoading) {
        showProgress(isLoading, GetView(), mProgressView);
    }

    public void setTitle(String title) {
        mActionBarToolbar.setTitle(title);
        toolBarTitle.setText(title);
    }

    public void setTitle2(String title2) {
        toolBarTitle2.setVisibility(title2 != null ? View.VISIBLE : View.GONE);
        mActionBarToolbar.setTitle(title2);
        toolBarTitle2.setText(title2);
    }

    @Override
    public void SetFabIcon(int res) {
        mntBtn.setImageDrawable(getResources().getDrawable(res));
    }

    @Override
    public void FabVisible(boolean show) {
        if (fabLayout != null)
            fabLayout.setVisibility((show) ? View.VISIBLE : View.GONE);
    }
}
