package ru.t.atomhack.Views;

import android.content.Context;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.Fragments.BaseFragment;


public interface IMain extends IBaseView {
    TabLayout getMaterialupTabs();

    void replaceFragment(BaseFragment fragment);

    Context getContext();

    void finish();

    void setTitle(String title);

    void setTitle2(String title2);

    CharSequence getTitle();

    void SetFabIcon(int res);

    void FabVisible(boolean show);

    void setFabClickListener(IOnCallBackListener fabClickListener);
}
