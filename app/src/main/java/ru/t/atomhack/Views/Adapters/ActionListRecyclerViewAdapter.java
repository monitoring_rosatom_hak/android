package ru.t.atomhack.Views.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.Models.ActionItem;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;


public class ActionListRecyclerViewAdapter extends AbstractRecyclerViewAdapter<ActionListRecyclerViewAdapter.ViewHolder, ActionItem> {

    public ActionListRecyclerViewAdapter(List<ActionItem> list, IOnCallBackListener callBackListener) {
        super(list, callBackListener);
    }

    @Override
    protected View getView(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_action, parent, false);
    }

    @Override
    protected ViewHolder initHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    protected void populate(ViewHolder holder, int position) {
        ActionItem mItem = mValue.get(position);
        holder.button.setText(mItem.getTitle());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.button)      Button button;
        @OnClick({R.id.button})
        void onRowClick(){
            int position = getLayoutPosition();
            ActionItem clickedItem = mValue.get(position);
            if (callBack!=null)
                callBack.callBack(clickedItem);
        }
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
