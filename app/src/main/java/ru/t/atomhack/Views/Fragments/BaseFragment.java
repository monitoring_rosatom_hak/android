package ru.t.atomhack.Views.Fragments;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;

import javax.inject.Inject;

import ru.t.atomhack.DI.Components.DaggerPresenterComponent;
import ru.t.atomhack.Presenters.Interfaces.IPresenter;
import ru.t.atomhack.Views.IMain;

import static ru.t.atomhack.App.getComponent;

public abstract class BaseFragment extends Fragment {
    @Inject
    IPresenter mPresenter;
    protected static final String DESCRIPTION = "DESCRIPTION";

    protected View mView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mPresenter = DaggerPresenterComponent.builder()
                .appComponent(getComponent())
                .build().getPresenter();
    }

    public String getTitle() {
        return parentView.getTitle().toString();
    }

    public IMain getParentView() {
        return parentView;
    }
    public void setParentView(IMain _parentView) {
        parentView = _parentView;
        if (parentView != null)
            parentView.FabVisible(false);
    }
    private IMain parentView;

    public View GetView() {
        return parentView.GetView();
    }

    public void setTitle(CharSequence title){
        parentView.setTitle2(title.toString());
    }

    public void showError(Object e) {
        parentView.showError(e);
    }

    public void showLoading(boolean isLoading) {
        parentView.showLoading(isLoading);
    }

}
