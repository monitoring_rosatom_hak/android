package ru.t.atomhack.Views.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.RowItem;
import ru.t.atomhack.Presenters.CommercialPresenter;
import ru.t.atomhack.Presenters.LocationsPresenter;
import ru.t.atomhack.R;

import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showAlert;
import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showSelectDialog;


public class LocationRowListRecyclerViewAdapter extends AbstractRecyclerViewAdapter<LocationRowListRecyclerViewAdapter.ViewHolder, RowItem> {

    private final LocationsPresenter _presenter;

    public LocationRowListRecyclerViewAdapter(List<RowItem> list, LocationsPresenter presenter) {
        super(list, null);
        _presenter = presenter;
    }

    @Override
    protected View getView(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);
    }

    @Override
    protected ViewHolder initHolder(View view) {
        return new ViewHolder(view);
    }

    public void clearRows() {
        for (RowItem row : mValue) {
            row.setName("");
        }
        notifyDataSetChanged();
    }

    @Override
    protected void populate(ViewHolder holder, int position) {
        RowItem mItem = mValue.get(position);
        if (mItem != null) {
            holder.title.setText(mItem.getTitle());
            holder.name.setText(mItem.getName());
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.name)
        TextView name;

        @OnClick({R.id.row})
        void onRowClick() {
            int position = getLayoutPosition();
            RowItem clickedItem = mValue.get(position);
            if (_ctx != null) {
                View d = ((Activity) _ctx).getLayoutInflater().inflate(R.layout.text_view_dialod, null);
                EditText editTextDescription = d.findViewById(R.id.editTextDescription);
                if (editTextDescription != null) {
                    editTextDescription.setText((CharSequence) name.getText().toString());
                }
                showAlert(_ctx, "Указание адреса", "", o -> {
                            if (editTextDescription != null) {
                                name.setText(editTextDescription.getText().toString());
                                clickedItem.setName(editTextDescription.getText().toString());
                                _presenter.setDescription(editTextDescription.getText().toString());
                                return true;
                            }
                            return false;
                        }
                        , d);
            }
            if (callBack != null)
                callBack.callBack(clickedItem);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
