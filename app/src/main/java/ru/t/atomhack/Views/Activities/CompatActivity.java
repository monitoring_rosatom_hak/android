package ru.t.atomhack.Views.Activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;


import ru.t.atomhack.DI.Components.ActivityComponent;
import ru.t.atomhack.DI.Components.DaggerActivityComponent;
import ru.t.atomhack.DI.Components.DaggerPresenterComponent;
import ru.t.atomhack.DI.Components.PresenterComponent;
import ru.t.atomhack.DI.Modules.ActivityModule;
import ru.t.atomhack.DI.Modules.PresenterModule;
import ru.t.atomhack.Presenters.Interfaces.IPresenter;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.ISlidingMenu;

import static ru.t.atomhack.App.getComponent;


abstract class CompatActivity extends AppCompatActivity implements IBaseView {

    IPresenter mPresenter;

    protected PresenterComponent mPresenterComponent;
    protected ActivityComponent activityComponent;
    protected ISlidingMenu slidingMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPresenterComponent = DaggerPresenterComponent.builder()
                .appComponent(getComponent())
                .build();
        activityComponent = DaggerActivityComponent.builder()
                .activityModule(new ActivityModule(this))
                .build();
        slidingMenu = activityComponent.getSlidingMenuView();

        mPresenter = mPresenterComponent.getPresenter();


    }

    @Override
    public void showError(Object e) {
        if (e != null){
            String error = e.toString();
            if (GetView()!=null) {
                Snackbar snackbar = Snackbar.make(GetView(), error, Snackbar.LENGTH_LONG);
                snackbar.setAction("X", v -> snackbar.dismiss());
                snackbar.show();
            }else
                Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }


    }
}
