package ru.t.atomhack.Views;

import ru.t.atomhack.Models.DTO.Request;

public interface IFullPhoto extends IBaseView {
    void SetImageBitmap(Request bitmap);
    void finish();
}
