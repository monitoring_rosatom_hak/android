package ru.t.atomhack.Views.Activities;

import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.Adapters.PagerAdapter;
import ru.t.atomhack.Views.Fragments.BaseFragment;
import ru.t.atomhack.Views.Fragments.MapFragment;
import ru.t.atomhack.Views.Fragments.RequestFragment;
import ru.t.atomhack.Views.IMain;

import static ru.t.atomhack.Utils.GlobalUtils.REQUEST_PERMISSIONS_REQUEST_CODE;
import static ru.t.atomhack.Utils.GlobalUtils.showProgress;

public class Main extends CompatActivity implements IMain {
    private static String TAG = "TAG";
    private FragmentManager fragmentManager;
    @BindView(R.id.progressBar)
    View mProgressView;
    @BindView(R.id.toolbar_actionbar)
    Toolbar mActionBarToolbar;
    @BindView(R.id.toolbar_title)
    TextView toolBarTitle;
    @BindView(R.id.toolbar_title2)
    TextView toolBarTitle2;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.mntBtn)
    FloatingActionButton mntBtn;
    public TabLayout getMaterialupTabs() {
        return materialupTabs;
    }

    @BindView(R.id.materialupTabs)      TabLayout materialupTabs;
    @BindView(R.id.fab_layout)
    ConstraintLayout fabLayout;
    @OnClick(R.id.mntBtn)
    void onMntBtnClick(View v) {
        if (fabClickListener != null)
            fabClickListener.callBack(v);
    }
    private PagerAdapter pagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        showLoading(false);
        fragmentManager = getSupportFragmentManager();
        pager.setAdapter(new PagerAdapter(getSupportFragmentManager()));
        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(materialupTabs));
        materialupTabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                PagerAdapter pagerAdapter = (PagerAdapter) pager.getAdapter();
                if (tab.getPosition() < 0) return;
                if (pagerAdapter != null && pagerAdapter.getCount() > tab.getPosition())
                    pager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        AddPage("К порядку!", RequestFragment.newInstance(), R.drawable.ic_menu_offline);
        AddPage("Карта", MapFragment.newInstance(), R.drawable.ic_menu_mapmode);
        /*fragmentManager = getSupportFragmentManager();
        BaseFragment fragment = (BaseFragment) fragmentManager.findFragmentByTag(TAG);
        if (fragment == null) {
            replaceFragment(MapFragment.newInstance());
        }else
            fragment.setParentView(this);*/
    }

    public void setFabClickListener(IOnCallBackListener fabClickListener) {
        this.fabClickListener = fabClickListener;
    }

    private IOnCallBackListener fabClickListener;

    public void AddPage(String title, BaseFragment fragment, int img) {
        if (pagerAdapter != null) {
            for (int i = 0; i < pagerAdapter.getCount(); i++) {
                if (pagerAdapter.getPageTitle(i).equals(title))
                    return;
            }
        }
        //fragment.Init(title, this, img);
        TabLayout tabLayout = materialupTabs;
        if (tabLayout == null) return;
        int i = tabLayout.getTabCount();
        tabLayout.addTab(tabLayout.newTab().setCustomView(R.layout.custom_tab));
        TabLayout.Tab tabLayoutTabAt = tabLayout.getTabAt(i);
        TextView tab = null;
        if (tabLayoutTabAt != null)
            tab = (TextView) tabLayoutTabAt.getCustomView();
        if (tab != null) {
            tab.setText(title);
            tab.setCompoundDrawablesWithIntrinsicBounds(0, img, 0, 0);
        }
        PagerAdapter pagerAdapter = (PagerAdapter) pager.getAdapter();
        //pagerAdapter.notifyDataSetChanged();
        if (pagerAdapter != null) {
            //pagerAdapter = new CallPagerAdapter(getFragmentManager());
            //pager.setAdapter(pagerAdapter);
            //}else{
            pagerAdapter.addFrag(fragment);
            //if (pagerAdapter.getCount()>0)
            //    pagerAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (int i = 0; i < grantResults.length; i++) {
            permissionsToRequest.add(permissions[i]);
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    this,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (Fragment fragment : fragmentManager.getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void replaceFragment(BaseFragment fragment) {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void finish() {
        finish();
    }

    @Override
    public void setTitle(String title) {
        mActionBarToolbar.setTitle(title);
        toolBarTitle.setText(title);
    }

    @Override
    public View GetView() {
        return findViewById(R.id.container);
    }

    @Override
    public void showLoading(boolean isLoading) {
        showProgress(isLoading, GetView(), mProgressView);
    }

    public void setTitle2(String title2) {
        toolBarTitle2.setVisibility(title2 != null ? View.VISIBLE : View.GONE);
        mActionBarToolbar.setTitle(title2);
        toolBarTitle2.setText(title2);
    }

    @Override
    public void SetFabIcon(int res) {
        mntBtn.setImageDrawable(getResources().getDrawable(res));
    }

    @Override
    public void FabVisible(boolean show) {
        fabLayout.setVisibility((show) ? View.VISIBLE : View.GONE);
    }
}
