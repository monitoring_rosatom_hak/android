package ru.t.atomhack.Views.Fragments;

import android.Manifest;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.OnlineTileSourceBase;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.util.MapTileIndex;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.t.atomhack.Models.DTO.PointItem;
import ru.t.atomhack.Presenters.MapPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Views.IBaseView;
import ru.t.atomhack.Views.IMain;

import static ru.t.atomhack.Utils.GlobalUtils.createMarkerIcon;
import static ru.t.atomhack.Utils.GlobalUtils.requestPermissionsIfNecessary;


public class MapFragment extends BaseFragment implements IBaseView {

    @BindView(R.id.map)
    MapView map;
    private MapPresenter _mapPresenter;
    private ArrayList<PointItem> evenetItemList;

    public MapFragment() {
        // Required empty public constructor//
    }

    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Карта событий");
        _mapPresenter = new MapPresenter(this);
        _mapPresenter.setParentView((IMain) getParentView());

        TabLayout tabs = getParentView().getMaterialupTabs();
        tabs.removeAllTabs();
        tabs.clearOnTabSelectedListeners();
        AddTab(tabs, "Радиация", R.drawable.ic_radiation);
        AddTab(tabs, "Пожары", R.drawable.ic_fire);
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() < 0) return;
                switch (tab.getPosition()) {
                    case 0:
                        _mapPresenter.LoadEventList("radiation", o -> {
                            if (o != null && o instanceof ArrayList) {
                                evenetItemList = (ArrayList<PointItem>) o;
                                populate();
                                return true;
                            }
                            return false;
                        });
                        break;
                    case 1:
                        _mapPresenter.LoadEventList("fire", o -> {
                            if (o != null && o instanceof ArrayList) {
                                evenetItemList = (ArrayList<PointItem>) o;
                                populate();
                                return true;
                            }
                            return false;
                        });
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        _mapPresenter.LoadEventList("radiation", o -> {
            if (o != null && o instanceof ArrayList) {
                evenetItemList = (ArrayList<PointItem>) o;
                populate();
                return true;
            }
            return false;
        });
    }

    private void AddTab(TabLayout tabs, String title, int img) {
        tabs.setVisibility(View.VISIBLE);
        tabs.addTab(tabs.newTab().setCustomView(R.layout.custom_tab));
        int i = tabs.getTabCount() - 1;
        TabLayout.Tab tabLayoutTabAt = tabs.getTabAt(i);
        TextView tab = null;
        if (tabLayoutTabAt != null)
            tab = (TextView) tabLayoutTabAt.getCustomView();
        if (tab != null) {
            tab.setText(title);
            tab.setCompoundDrawablesWithIntrinsicBounds(0, img, 0, 0);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        map.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        map.onPause();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_map, container, false);
        ButterKnife.bind(this, mView);
        mPresenter.setView(this);


        return mView;
    }

    private void populate() {
        if (map == null || evenetItemList == null) {
            return;
        }
        map.getOverlays().clear();
        for (PointItem event : evenetItemList) {
            Resources resurses = getContext().getResources();
            Marker my_marker = new Marker(map);
            //my_marker.setPosition(new GeoPoint(56.58934, 84.92727));
            my_marker.setPosition(new GeoPoint(event.getLatitude(), event.getLongitude()));
            my_marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
            my_marker.setIcon(createMarkerIcon(resurses.getDrawable((event.getType().equals("radiation")) ? R.drawable.shape_oval : R.drawable.ic_fire_red), event.getName(), resurses.getDisplayMetrics().density));
            my_marker.setTitle(event.getDescription());
            my_marker.setPanToView(true);
            map.getOverlays().add(my_marker);
        }
        map.invalidate();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        requestPermissionsIfNecessary(getActivity(), new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE
        });

        Context ctx = view.getContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));
        map.getTileProvider().clearTileCache();
        Configuration.getInstance().setCacheMapTileCount((short) 12);
        Configuration.getInstance().setCacheMapTileOvershoot((short) 12);
        // Create a custom tile source
        map.setTileSource(new OnlineTileSourceBase("", 1, 20, 512, ".png",
                new String[]{"https://a.tile.openstreetmap.org/"}) {
            @Override
            public String getTileURLString(long pMapTileIndex) {
                return getBaseUrl()
                        + MapTileIndex.getZoom(pMapTileIndex)
                        + "/" + MapTileIndex.getX(pMapTileIndex)
                        + "/" + MapTileIndex.getY(pMapTileIndex)
                        + mImageFilenameEnding;
            }
        });
        map.setMultiTouchControls(true);
        map.setClickable(true);

        IMapController mapController = map.getController();
        mapController.setZoom(13.5);
        GeoPoint startPoint = new GeoPoint(56.60056, 84.88639);
        mapController.setCenter(startPoint);
//        ScaleBarOverlay scala = new ScaleBarOverlay(map);
//        map.getOverlays().add(scala);
        populate();
    }

    public void createmarker() {
        if (map == null) {
            return;
        }
        Resources resurses = getContext().getResources();
        Marker my_marker = new Marker(map);
        //my_marker.setPosition(new GeoPoint(56.58934, 84.92727));
        my_marker.setPosition(new GeoPoint(56.60056, 84.88639));
        my_marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        my_marker.setIcon(createMarkerIcon(resurses.getDrawable(R.drawable.shape_oval), "1", resurses.getDisplayMetrics().density));
        my_marker.setTitle("Give it a title");
        my_marker.setPanToView(true);
        map.getOverlays().add(my_marker);
        map.invalidate();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof IMain) {
            setParentView((IMain) context);
        }
    }


}
