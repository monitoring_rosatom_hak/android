package ru.t.atomhack.Views.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.Models.ActionItem;
import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;


public class SettingsListRecyclerViewAdapter extends AbstractRecyclerViewAdapter<SettingsListRecyclerViewAdapter.ViewHolder, IdNameCheckItem> {

    public SettingsListRecyclerViewAdapter(List<IdNameCheckItem> list) {
        super(list, null);
    }

    @Override
    protected View getView(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_switch, parent, false);
    }

    public void setList(List<IdNameCheckItem> list) {
        mValue = list;
        notifyDataSetChanged();
    }

    @Override
    protected ViewHolder initHolder(View view) {
        return new ViewHolder(view);
    }

    @Override
    protected void populate(ViewHolder holder, int position) {
        IdNameCheckItem mItem = mValue.get(position);
        holder.switchItem.setText(mItem.getName());
        holder.switchItem.setChecked(mItem.getChecked());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.switchItem)
        Switch switchItem;

        @OnClick({R.id.switchItem})
        void onRowClick() {
            int position = getLayoutPosition();
            IdNameCheckItem clickedItem = mValue.get(position);
            clickedItem.setChecked(!clickedItem.getChecked());
            switchItem.setChecked(clickedItem.getChecked());
            if (callBack != null)
                callBack.callBack(position);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
