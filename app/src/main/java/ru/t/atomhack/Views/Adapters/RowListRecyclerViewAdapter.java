package ru.t.atomhack.Views.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.RowItem;
import ru.t.atomhack.Presenters.CommercialPresenter;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;

import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showAlert;
import static ru.t.atomhack.Utils.Dialogs.DialogsManager.showSelectDialog;


public class RowListRecyclerViewAdapter extends AbstractRecyclerViewAdapter<RowListRecyclerViewAdapter.ViewHolder, RowItem> {

    private final CommercialPresenter _presenter;

    public RowListRecyclerViewAdapter(List<RowItem> list, CommercialPresenter presenter) {
        super(list, null);
        _presenter = presenter;
    }

    @Override
    protected View getView(ViewGroup parent) {
        return LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_row, parent, false);
    }

    @Override
    protected ViewHolder initHolder(View view) {
        return new ViewHolder(view);
    }

    public void clearRows() {
        for (RowItem row : mValue) {
            row.setName("");
        }
        notifyDataSetChanged();
    }

    @Override
    protected void populate(ViewHolder holder, int position) {
        RowItem mItem = mValue.get(position);
        if (mItem!= null) {
            holder.title.setText(mItem.getTitle());
            holder.name.setText(mItem.getName());
        }


    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.name)
        TextView name;

        @OnClick({R.id.row})
        void onRowClick() {
            int position = getLayoutPosition();
            RowItem clickedItem = mValue.get(position);
            List<String> list = new ArrayList<>();
            switch (clickedItem.getType()) {
                case "service":
                    _presenter.LoadServiceList(o -> {
                        if (o != null && o instanceof ArrayList) {
                            int j = -1;
                            int k = -1;
                            ArrayList<CodeNameItem> servList = (ArrayList<CodeNameItem>) o;
                            list.clear();
                            for (CodeNameItem r : servList) {
                                list.add(r.getName());
                                if (r.getName().equals(clickedItem.getName())) k = j;
                                j++;
                            }

                            if (_ctx instanceof FragmentActivity)
                                showSelectDialog((FragmentActivity) _ctx, "Выбор услуги", list, k, i -> {
                                    if (i > -1) {
                                        name.setText(servList.get(i).getName());
                                        clickedItem.setName(servList.get(i).getName());
                                        _presenter.setServCode(servList.get(i).getCode());
                                    }

                                });
                        }
                        return false;
                    });
                    break;
                case "owner":
                    _presenter.LoadOwnerList(o -> {
                        if (o != null && o instanceof ArrayList) {
                            int j = -1;
                            int k = -1;
                            ArrayList<CodeNameItem> ownerList = (ArrayList<CodeNameItem>) o;
                            list.clear();
                            for (CodeNameItem r : ownerList) {
                                list.add(r.getName());
                                if (r.getName().equals(clickedItem.getName())) k = j;
                                j++;
                            }

                            if (_ctx instanceof FragmentActivity)
                                showSelectDialog((FragmentActivity) _ctx, "Выбор поставщика", list, k, i -> {
                                    if (i > -1) {
                                        name.setText(ownerList.get(i).getName());
                                        clickedItem.setName(ownerList.get(i).getName());
                                        _presenter.setOwner(ownerList.get(i).getCode());
                                    }
                                });
                        }
                        return false;
                    });
                    break;
                case "tariff":
                    _presenter.LoadTariffList(o -> {
                        if (o != null && o instanceof ArrayList) {
                            int j = -1;
                            int k = -1;
                            ArrayList<IdNameDescrItem> tariffList = (ArrayList<IdNameDescrItem>) o;
                            list.clear();
                            for (IdNameDescrItem r : tariffList) {
                                list.add(r.getName());
                                if (r.getName().equals(clickedItem.getName())) k = j;
                                j++;
                            }

                            if (_ctx instanceof FragmentActivity)
                                showSelectDialog((FragmentActivity) _ctx, "Выбор тарифа", list, k, i -> {
                                    if (i > -1) {
                                        name.setText(tariffList.get(i).getName());
                                        clickedItem.setName(tariffList.get(i).getName());
                                        _presenter.setTariff(tariffList.get(i));
                                    }
                                });
                        }
                        return false;
                    });
                    break;
                case "description":
                    if (_ctx != null) {
                        View d = ((Activity) _ctx).getLayoutInflater().inflate(R.layout.text_view_dialod, null);
                        EditText editTextDescription = d.findViewById(R.id.editTextDescription);
                        if (editTextDescription != null) {
                            editTextDescription.setText((CharSequence) name.getText().toString());
                        }
                        showAlert(_ctx, "Указание примечания", "", o -> {
                                    if (editTextDescription != null) {
                                        name.setText(editTextDescription.getText().toString());
                                        clickedItem.setName(editTextDescription.getText().toString());
                                        _presenter.setDescription(editTextDescription.getText().toString());
                                        return true;
                                    }
                                    return false;
                                }
                                , d);
                    }

                    break;
            }
            if (callBack != null)
                callBack.callBack(clickedItem);
        }

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }


}
