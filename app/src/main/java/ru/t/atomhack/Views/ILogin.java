package ru.t.atomhack.Views;

public interface ILogin extends IBaseView {
    void showLoginError(String error);
    void showPasswordError(String error);
    String getUserName();
    String getPassword();
}
