package ru.t.atomhack.Utils.Dialogs;

public interface IDialogManager {
    void alert(String title, String message);
    void confirm(String text, final DialogsManager.Act a);
}
