package ru.t.atomhack.Utils.Dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;


class RadioButtonAdapter extends RecyclerView.Adapter<RadioButtonAdapter.ViewHolder> {
    private List<String> _objects;

    public void setCallBackListener(IOnCallBackListener callBackListener) {
        this.callBackListener = callBackListener;
    }

    private IOnCallBackListener callBackListener;

    void setSelectedIdx(int selectedIdx) {
        this.selectedIdx = selectedIdx;
    }

    private int selectedIdx = -1;

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.radioButton)
        RadioButton radioButton;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.radioButton)
        public void onClick(View view) {
            selectedIdx = getAdapterPosition();
            notifyDataSetChanged();
            if (callBackListener != null) callBackListener.callBack(selectedIdx);
            //dismiss();
        }
    }

    RadioButtonAdapter(List<String> objects) {
        _objects = objects;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_radio_button, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String item = _objects.get(position);
        holder.radioButton.setText(item);
        holder.radioButton.setChecked(selectedIdx == position);
    }

    int getSelectedIdx() {
        return selectedIdx;
    }

    @Override
    public int getItemCount() {
        return _objects != null ? _objects.size() : 0;
    }
}

public class CustomDialogFragment extends DialogFragment {
    private IOnCallBackListener dismissListener;

    public void setLayout(int layout) {
        this.layout = layout;
    }

    private int layout;

    public void setOnDismissListener(IOnCallBackListener dismissListener) {
        this.dismissListener = dismissListener;
    }

    public void setCallBackListener(IOnCallBackListener callBackListener) {
        this.callBackListener = callBackListener;
    }

    private IOnCallBackListener callBackListener;

    public void setList(List<String> list) {
        this.list = list;
    }

    private List<String> list;

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public void setSelectedIdx(int selectedIdx) {
        this.selectedIdx = selectedIdx;
    }

    private int selectedIdx;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        layout = R.layout.dialog_select;
        View view = inflater.inflate(layout, container, false);

        TextView toolbar_title = view.findViewById(R.id.toolbar_title);
        toolbar_title.setText(title);

        RadioButtonAdapter adapter = new RadioButtonAdapter(list);
        RecyclerView listView = view.findViewById(R.id.list_view);
        listView.setAdapter(adapter);
        if (selectedIdx > -1) adapter.setSelectedIdx(selectedIdx);

        view.findViewById(R.id.cancel_click).setOnClickListener(view1 -> {
            if (callBackListener != null) callBackListener.callBack(null);
            CustomDialogFragment.this.close_dialog();
            //dismiss();
        });
        adapter.setCallBackListener((IOnCallBackListener) o -> {
            if (callBackListener != null && adapter.getSelectedIdx() > -1)
                callBackListener.callBack(adapter.getSelectedIdx());
            close_dialog();
            return false;
            //dismiss();
        });
        //setCancelable(false);
        return view;
    }

    private void close_dialog() {
        Fragment prev = null;
        if (getActivity() != null)
            prev = getActivity().getSupportFragmentManager().findFragmentByTag("custom_dialog");
        if (prev != null) {
            DialogFragment df = (DialogFragment) prev;
            df.dismiss();
        }
        if (this.dismissListener != null)
            dismissListener.callBack(null);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if (this.dismissListener != null)
            dismissListener.callBack(dialog);
        super.onDismiss(dialog);

    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@NonNull Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

}
