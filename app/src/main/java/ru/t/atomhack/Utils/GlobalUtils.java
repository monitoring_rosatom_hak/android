package ru.t.atomhack.Utils;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class GlobalUtils {

    public static final int REQUEST_PERMISSIONS_REQUEST_CODE = 1;

    public static void requestPermissionsIfNecessary(Context ctx, String[] permissions) {
        ArrayList<String> permissionsToRequest = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(ctx, permission)
                    != PackageManager.PERMISSION_GRANTED) {
                // Permission is not granted
                permissionsToRequest.add(permission);
            }
        }
        if (permissionsToRequest.size() > 0) {
            ActivityCompat.requestPermissions(
                    (Activity) ctx,
                    permissionsToRequest.toArray(new String[0]),
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    public static Drawable createMarkerIcon(Drawable backgroundImage, String text, float density) {
        float width = backgroundImage.getMinimumWidth() / density;
        float height = backgroundImage.getMinimumHeight() / density;

        Bitmap canvasBitmap = Bitmap.createBitmap((int) width, (int) height,
                Bitmap.Config.ARGB_8888);
        // Create a canvas, that will draw on to canvasBitmap.
        Canvas imageCanvas = new Canvas(canvasBitmap);

        // Set up the paint for use with our Canvas
        Paint imagePaint = new Paint();
        imagePaint.setTextAlign(Paint.Align.CENTER);
        imagePaint.setTextSize(14f);
        imagePaint.setColor(Color.parseColor("#000000"));


        // Draw the image to our canvas
        backgroundImage.draw(imageCanvas);
        // Draw the text on top of our image
        imageCanvas.drawText(text, (float) (width / 2), (float) (height / 1.5), imagePaint);

        // Combine background and text to a LayerDrawable
        return new LayerDrawable(
                new Drawable[]{backgroundImage, new BitmapDrawable(canvasBitmap)});
    }

    public static Date ToDate(String dt) {
        return ToDate(dt, "dd.MM.yyyy HH:mm:ss");
    }

    public static Date ToDate(String dt, String format) {
        try {
            return new SimpleDateFormat(format, Locale.ROOT).parse(dt);
        } catch (Exception e) {
            //return new Date(1,1,1,0,0,0);
            //return ToDate("01.01.0001 00:00:00");
            return null;
        }
    }

    public static double GetDouble(String s) {
        double result = 0;
        if (s != null && !TextUtils.isEmpty(s)) {
            try {
                result = Double.valueOf(s);
            } catch (NumberFormatException e) {
                System.out.println("parse value is not valid : " + e);
            }
        }
        return result;
    }

    public static Long GetLong(String s) {
        Long result = Long.valueOf(0);
        if (s != null && !TextUtils.isEmpty(s)) {
            try {
                result = Long.parseLong(s);
            } catch (NumberFormatException e) {
                System.out.println("parse value is not valid : " + e);
            }
        }
        return result;
    }

    public static int GetInt(String s) {
        int result = 0;
        if (s != null && !TextUtils.isEmpty(s)) {
            try {
                result = Integer.parseInt(s);
            } catch (NumberFormatException e) {
                System.out.println("parse value is not valid : " + e);
            }
        }
        return result;
    }

    public static String GetText(TextView view) {
        return (view != null && view.getText() != null) ? view.getText().toString() : "";
    }

    public static String FromDate(Date dt) {
        return FromDate(dt, "dd.MM.yyyy HH:mm:ss");
    }

    public static String FromDate(Date dt, String format) {
        if (dt != null) {
            String dtStr;
            try {
                dtStr = new SimpleDateFormat(format, Locale.ROOT).format(dt);
            } catch (Exception exception) {
                return "";
            }


            return dtStr.contains("01.01.1970") || dtStr.contains("01.01.0001") ? "" : dtStr;
        }
        return "";
    }

    public static boolean isNetworkConnected(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = null;
        if (cm != null) {
            ni = cm.getActiveNetworkInfo();
        }
        return ni != null;
    }

    public static void showProgress(final boolean show, final View formView, View progressView) {

        if (progressView == null)
            return;

        int shortAnimTime = progressView.getContext().getResources().getInteger(android.R.integer.config_shortAnimTime);
        if (formView != null) {
            formView.setVisibility(show ? View.GONE : View.VISIBLE);
            formView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    formView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });
        }
        progressView.setVisibility(show ? View.VISIBLE : View.GONE);
        final View finalProgressView = progressView;
        progressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                finalProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }
}
