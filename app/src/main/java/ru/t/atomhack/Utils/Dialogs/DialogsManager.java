package ru.t.atomhack.Utils.Dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

import me.drakeet.materialdialog.MaterialDialog;
import ru.t.atomhack.R;
import ru.t.atomhack.Utils.IOnCallBackListener;
import ru.t.atomhack.Views.Activities.Login;

public class DialogsManager {

    public interface Select {
        void select(int i);
    }
    public abstract static class  TriAct {
        public TriAct(String title1, String title2, String title3) {
            this.title1 = title1;
            this.title2 = title2;
            this.title3 = title3;
        }

        public TriAct(String title1, String title2) {
            this.title1 = title1;
            this.title2 = title2;
            this.title3 = "Отмена";
        }

        public String title1;
        public String title2;
        public String title3;

        public abstract void positive1Click();
        public abstract void positive2Click();
        public abstract void negativeClick();
    }
    public interface Act {
        void positiveClick();
        void negativeClick();
    }
    public static void showConfirmTriDialog(Context mContext, String title, String message, final TriAct a) {
        if (((Activity) mContext).isFinishing()) return;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        if (message!=null&&!message.trim().isEmpty())
            alertDialogBuilder.setMessage(Html.fromHtml(message));
        if (title!=null&&!title.trim().isEmpty())
            alertDialogBuilder.setTitle(Html.fromHtml(title));
        alertDialogBuilder.setNegativeButton(a.title3, (dialogInterface, i) -> {
            a.negativeClick();
            dialogInterface.dismiss();
        });
        alertDialogBuilder.setPositiveButton(a.title1, (dialogInterface, i) -> {
            a.positive1Click();
            dialogInterface.dismiss();
        });
        alertDialogBuilder.setNeutralButton(a.title2, (dialogInterface, i) -> {
            a.positive2Click();
            dialogInterface.dismiss();
        });
        alertDialogBuilder.show();
    }

    public static void showConfirmDialog(Context mContext, String title, String message, final Act a) {
        if (((Activity) mContext).isFinishing()) return;
        MaterialDialog mMaterialDialog = new MaterialDialog(mContext);
        if (message!=null&&!message.trim().isEmpty())
            mMaterialDialog.setMessage(Html.fromHtml(message));
        if (title!=null&&!title.trim().isEmpty())
            mMaterialDialog.setTitle(Html.fromHtml(title));
        mMaterialDialog.setNegativeButton(mContext.getString(R.string.cancel), v -> {
            a.negativeClick();
            mMaterialDialog.dismiss();
        });
        mMaterialDialog.setPositiveButton(mContext.getString(R.string.yes), v -> {
            a.positiveClick();
            mMaterialDialog.dismiss();
        });
        mMaterialDialog.show();
    }

    public static void showConfirmDialog(Context mContext, String text, final Act a) {
        showConfirmDialog(mContext,null, text, a);
    }

    public static void showAlert(Context mContext, String title, View view) {
        showAlert(mContext, title, null, false);
    }


    public static void showAlert(Context mContext, String title, String message) {
        showAlert(mContext, title, message, false);
    }

    public static void showAlert(Context mContext, String title, String message, boolean restart) {
        showAlert(mContext, title, message, null, null);

    }
    public static void showAlert(Context mContext, String title, String message, IOnCallBackListener callBackListener, View view) {
        if (((Activity) mContext).isFinishing()) return;
        final MaterialDialog mMaterialDialog = new MaterialDialog(mContext);
        mMaterialDialog.setCanceledOnTouchOutside(true);
        if (view!=null) mMaterialDialog.setContentView(view);
        mMaterialDialog.setMessage(message).setTitle(title)
                .setCanceledOnTouchOutside(false)
                .setPositiveButton("OK", o ->  {
                    mMaterialDialog.dismiss();
                    if (callBackListener!=null){
                        callBackListener.callBack(null);
                    }
                });
        mMaterialDialog.show();
    }

    public static void showSelectDialog(FragmentActivity mContext, String title, List<String> list, final Select a) {
        showSelectDialog(mContext, title, list, -1, a);
    }
    public static void showSelectDialog(FragmentActivity mContext, String title, List<String> list, int selectedIdx, Select a) {
        showSelectDialog(mContext, title, list, selectedIdx, a, null);
    }

    public static void showSelectDialog(FragmentActivity mContext, String title, List<String> list, int selectedIdx, Select a, IOnCallBackListener onDissmisListener) {
        if (((Activity) mContext).isFinishing()) return;
        if (list.size()>0) {
            FragmentManager fragmentManager = mContext.getSupportFragmentManager();
            CustomDialogFragment newFragment = new CustomDialogFragment();
            newFragment.setTitle(title);
            newFragment.setList(list);
            newFragment.setSelectedIdx(selectedIdx);
            newFragment.setCallBackListener(o -> {
                a.select((o != null) ? (int) o : -1);
                return true;
            });
            if (onDissmisListener != null)
                newFragment.setOnDismissListener(o -> onDissmisListener.callBack(o));
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(android.R.id.content, newFragment, "custom_dialog").addToBackStack(null).commit();

        }
    }
    public static void showAlertDialogItemsSingle(Context mContext, String title, String list[], final Select a) {
        if (((Activity) mContext).isFinishing()) return;
        if (list.length>0) {
            AlertDialog.Builder adb = new AlertDialog.Builder(mContext);
            adb.setTitle(title);
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.select_dialog_singlechoice);
            arrayAdapter.addAll(list);
            adb.setSingleChoiceItems(arrayAdapter, -1, (dialogInterface, i) -> {
                a.select(i);
                dialogInterface.dismiss();
            });
            adb.show();
        }
    }
}
