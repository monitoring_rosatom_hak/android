package ru.t.atomhack.Models.PARAM;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CodeParams extends SessionParams implements Serializable {

    public CodeParams(String code, String authKey) {
        super(authKey);
        code = code;
    }

    @SerializedName("code")
    public String code;

    @Override
    public String ToString() {
        return gson.toJson(this);
    }
}
