package ru.t.atomhack.Models.PARAM;

import com.google.gson.annotations.SerializedName;

public class SaveOrderParams extends ServiceOwnerParams {
    public SaveOrderParams(String service, String owner, Integer tariff, String description, String authKey) {
        super(service, owner, authKey);
        this.description = description;
        this.tariff = tariff;
    }

    @SerializedName("tariff")
    private Integer tariff;

    @SerializedName("description")
    private String description;



    @Override
    public String ToString() {
        return gson.toJson(this);
    }
}
