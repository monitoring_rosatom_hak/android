package ru.t.atomhack.Models.Api;

import retrofit2.Retrofit;


public class AuthApiModule extends BaseApiModule {

    public AuthApiModule() {}

    public static AuthApi getAuthApiInterface(String url) {
        Retrofit.Builder builder = getRetroBuilder(url);
        AuthApi authApi = builder.build().create(AuthApi.class);
        return authApi;
    }

}
