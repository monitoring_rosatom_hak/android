package ru.t.atomhack.Models;

import ru.t.atomhack.Models.DTO.AuthResult;
import ru.t.atomhack.Models.PARAM.AuthParams;
import ru.t.atomhack.Models.PARAM.LogoutParams;
import rx.Observable;

public interface AuthModel {
    Observable<AuthResult> LogOn(AuthParams params);
    Observable<NullClass> LogOut(LogoutParams params);
    void setSessionId(String sessionId);
    String getSessionId();
}
