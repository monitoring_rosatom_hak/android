package ru.t.atomhack.Models.PARAM;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServiceOwnerParams extends SessionParams implements Serializable {

    public ServiceOwnerParams(String service, String owner, String authKey) {
        super(authKey);
        service = service;
        owner = owner;
    }

    @SerializedName("owner")
    public String owner;

    @SerializedName("service")
    public String service;

    @Override
    public String ToString() {
        return gson.toJson(this);
    }
}
