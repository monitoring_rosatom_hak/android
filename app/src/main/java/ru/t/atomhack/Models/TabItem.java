package ru.t.atomhack.Models;

import android.os.Parcel;
import android.os.Parcelable;

import ru.t.atomhack.Views.Fragments.BaseFragment;

public class TabItem implements Parcelable {
    public TabItem(String code, String title, BaseFragment className, int icon) {
        Code = code;
        Title = title;
        ClassName = className;
        Icon = icon;
        IsComplit = false;
    }

    public String Code;
    public String Title;
    public BaseFragment ClassName;
    public int Icon;
    public boolean IsComplit;

    private TabItem(Parcel in) {
        Code = in.readString();
        Title = in.readString();
        Icon = in.readInt();
        IsComplit = in.readByte() != 0;
    }

    public static final Creator<TabItem> CREATOR = new Creator<TabItem>() {
        @Override
        public TabItem createFromParcel(Parcel in) {
            return new TabItem(in);
        }

        @Override
        public TabItem[] newArray(int size) {
            return new TabItem[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(Code);
        parcel.writeString(Title);
        parcel.writeInt(Icon);
        parcel.writeByte((byte) (IsComplit ? 1 : 0));
    }
}
