package ru.t.atomhack.Models.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class IdNameItem implements Serializable {


    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public IdNameItem(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;
}
