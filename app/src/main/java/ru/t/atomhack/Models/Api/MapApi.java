package ru.t.atomhack.Models.Api;


import java.util.ArrayList;
import java.util.Map;

import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Query;
import ru.t.atomhack.Models.DTO.PointItem;
import rx.Observable;


public interface MapApi {
    @GET("event_list")
    Observable<ArrayList<PointItem>> GetEventList(@Query("type") String type, @HeaderMap Map<String, String> headers);
}
