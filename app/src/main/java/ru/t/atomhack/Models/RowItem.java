package ru.t.atomhack.Models;

import java.util.ArrayList;

import ru.t.atomhack.Views.Fragments.BaseFragment;

public class RowItem {
    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }
    public RowItem(String title) {
        this.title = title;
        this.type = null;
    }



    public RowItem(String title, String type) {
        this.title = title;
        this.type = type;
    }

    private String title;
    private String type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
}
