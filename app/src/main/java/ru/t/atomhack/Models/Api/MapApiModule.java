package ru.t.atomhack.Models.Api;

import retrofit2.Retrofit;

public final class MapApiModule extends BaseApiModule {

    public MapApiModule() {
    }

    public static MapApi getRestApiInterface(String url) {
        Retrofit.Builder builder = getRetroBuilder(url);
        MapApi mapApi = builder.build().create(MapApi.class);
        return mapApi;
    }

}
