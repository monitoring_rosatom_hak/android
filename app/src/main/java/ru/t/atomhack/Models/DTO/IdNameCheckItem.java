package ru.t.atomhack.Models.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IdNameCheckItem extends IdNameItem {

    public IdNameCheckItem(Integer id, String name, Boolean checked) {
        super(id, name);
        this.checked = checked;

    }

    public Boolean getChecked() {
        return checked == null ? false : checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }

    @SerializedName("checked")
    @Expose
    private Boolean checked;
}
