package ru.t.atomhack.Models.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataLayerResult {

    public DataLayerResult(Integer status, Integer code, String message) {
        Status = status;
        Code = code;
        Message = message;
    }

    @SerializedName("status")
    @Expose
    private Integer Status;

    @SerializedName("code")
    @Expose
    private Integer Code;

    @SerializedName("message")
    @Expose
    private String Message;

    @SerializedName("name")
    @Expose
    private String Name;

    public Integer getStatus() {
        return Status;
    }

    public Integer getCode() {
        return Code;
    }

    public String getMessage() {
        return Message;
    }

    public String getName() {
        return Name;
    }
}