package ru.t.atomhack.Models.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class IdNameDescrItem extends IdNameItem {

    public IdNameDescrItem(int id, String name, String desc) {
        super(id, name);
        this.descr = descr;

    }

    @SerializedName("descr")
    @Expose
    private String descr;
}
