package ru.t.atomhack.Models.Api;


import java.util.ArrayList;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.NullClass;
import rx.Observable;


public interface CommercialApi {
    @GET("services")
    Observable<ArrayList<CodeNameItem>> GetServices(@HeaderMap Map<String, String> headers);

    @GET("owners")
    Observable<ArrayList<CodeNameItem>> GetOwners(@Query("code") String code, @HeaderMap Map<String, String> headers);

    @GET("tariffs")
    Observable<ArrayList<IdNameDescrItem>> GetTariffs(@Query("service") String service, @Query("owner") String owner, @HeaderMap Map<String, String> headers);

    @POST("order")
    Observable<NullClass> SaveOrder(@Body RequestBody params, @HeaderMap Map<String, String> headers);

}
