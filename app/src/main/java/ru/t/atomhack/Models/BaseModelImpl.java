package ru.t.atomhack.Models;

import android.content.pm.PackageInstaller;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;
import rx.Scheduler;

class BaseModelImpl{
    protected final Observable.Transformer schedulersTransformer;
    @Inject
    @Named(ConstClass.UI_THREAD)
    Scheduler uiThread;
    @Inject
    @Named(ConstClass.IO_THREAD)
    Scheduler ioThread;

    protected Map<String, String> InitHeaders(SessionParams sessionParams){
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type","application/json");
        headers.put("Authorization","Bearer "+sessionParams.getAuthKey());
        return headers;
    }

    BaseModelImpl() {
        schedulersTransformer = new Observable.Transformer() {
            @Override
            public Object call(Object o) {
                return ((Observable) o)
                        .subscribeOn(ioThread)
                        .observeOn(uiThread)
                        .unsubscribeOn(ioThread);
            }
        };
    }

    @SuppressWarnings("unchecked")
    protected <T> Observable.Transformer<T, T> applySchedulers() {
        return (Observable.Transformer<T, T>) schedulersTransformer;
    }
}
