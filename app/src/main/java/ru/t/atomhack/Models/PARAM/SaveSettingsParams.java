package ru.t.atomhack.Models.PARAM;

import java.util.ArrayList;

import ru.t.atomhack.Models.DTO.IdNameCheckItem;

public class SaveSettingsParams extends SessionParams {

    public SaveSettingsParams(ArrayList<IdNameCheckItem> list, String authKey) {
        super(authKey);
        this.list = list;
    }

    private ArrayList<IdNameCheckItem> list;

    @Override
    public String ToString() {
        return gson.toJson(list);
    }
}
