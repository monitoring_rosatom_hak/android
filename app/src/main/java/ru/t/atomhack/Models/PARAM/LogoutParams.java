package ru.t.atomhack.Models.PARAM;

import com.google.gson.annotations.SerializedName;

public class LogoutParams extends BaseParams {

    //@SerializedName("ConnectionID")
    @SerializedName("token")
    private String token;

    public String getToken() {return this.token;}
    public void setToken(String token) {this.token = token;}

    @Override
    public String ToString() {
        return gson.toJson(this);
    }


}
