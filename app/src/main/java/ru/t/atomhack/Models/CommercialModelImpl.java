package ru.t.atomhack.Models;

import java.util.ArrayList;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.t.atomhack.App;
import ru.t.atomhack.Models.Api.CommercialApi;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SaveOrderParams;
import ru.t.atomhack.Models.PARAM.ServiceOwnerParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;


public class CommercialModelImpl extends BaseModelImpl implements CommercialModel {

    @Inject
    CommercialApi commercialApi;

    public CommercialModelImpl() {
        super();
        App.getComponent().inject(this);
    }

    @Override
    public Observable<ArrayList<CodeNameItem>> GetServices(SessionParams params) {
        return commercialApi.GetServices(InitHeaders(params)).compose(applySchedulers());
    }

    @Override
    public Observable<ArrayList<CodeNameItem>> GetOwners(CodeParams params) {
        return commercialApi.GetOwners(params.code, InitHeaders(params)).compose(applySchedulers());
    }

    @Override
    public Observable<ArrayList<IdNameDescrItem>> GetTariffs(ServiceOwnerParams params) {
        return commercialApi.GetTariffs(params.service, params.owner, InitHeaders(params)).compose(applySchedulers());
    }

    @Override
    public Observable<NullClass> SaveOrder(SaveOrderParams params) {
        return commercialApi.SaveOrder(RequestBody.create(MediaType.parse("text/plain"), params.ToString()), InitHeaders(params)).compose(applySchedulers());
    }
}
