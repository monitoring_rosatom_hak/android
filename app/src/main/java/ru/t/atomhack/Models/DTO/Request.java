package ru.t.atomhack.Models.DTO;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class Request {
    public String getImg() {
        return img;
    }

    public void clearImg() {
        this.img = null;
    }

    public Bitmap getBitmap() {
        byte[] bytes = Base64.decode(img, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
    }

    public Integer getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public Request(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitmap != null)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] bytes = stream.toByteArray();
        try {
            stream.flush();
            stream.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        this.img = Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public void setImg(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitmap != null)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
        byte[] bytes = stream.toByteArray();
        try {
            stream.flush();
            stream.close();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        this.img = Base64.encodeToString(bytes, Base64.DEFAULT);
    }

    public Request(String type, String description, String img) {
        this.type = type;
        this.description = description;
        this.img = img;
    }

    public void init(ru.t.atomhack.Models.DTO.Request request) {
        this.id = request.getId();
        this.type = request.getType();
        this.description = request.getDescription();
        this.img = request.getImg();
    }

    @SerializedName("id")
    @Expose
    private Integer id;

    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("img")
    @Expose
    private String img;

}
