package ru.t.atomhack.Models.Api;

import retrofit2.Retrofit;

public final class CommercialApiModule extends BaseApiModule {

    public CommercialApiModule() {
    }

    public static CommercialApi getRestApiInterface(String url) {
        Retrofit.Builder builder = getRetroBuilder(url);
        CommercialApi commercialApi = builder.build().create(CommercialApi.class);
        return commercialApi;
    }

}
