package ru.t.atomhack.Models.Api;

import retrofit2.Retrofit;

public final class RequestApiModule extends BaseApiModule {

    public RequestApiModule() {
    }

    public static RequestApi getRestApiInterface(String url) {
        Retrofit.Builder builder = getRetroBuilder(url);
        RequestApi requestApi = builder.build().create(RequestApi.class);
        return requestApi;
    }

}
