package ru.t.atomhack.Models;

import java.util.ArrayList;

import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SaveSettingsParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;

public interface SettingsModel {
    Observable<ArrayList<IdNameCheckItem>> GetSettings(CodeParams params);
    Observable<NullClass> SaveSettings(SaveSettingsParams params);
}
