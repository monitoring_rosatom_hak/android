package ru.t.atomhack.Models;

import java.util.ArrayList;

import ru.t.atomhack.Models.DTO.PointItem;
import ru.t.atomhack.Models.PARAM.CodeParams;
import rx.Observable;

public interface MapModel {
    Observable<ArrayList<PointItem>> GetEventList(CodeParams params);
}
