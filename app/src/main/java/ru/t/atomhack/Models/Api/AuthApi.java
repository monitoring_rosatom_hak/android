package ru.t.atomhack.Models.Api;


import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import ru.t.atomhack.Models.DTO.AuthResult;
import ru.t.atomhack.Models.NullClass;
import rx.Observable;


public interface AuthApi {
    @Headers("Content-Type: application/json")
    @POST("login")
    Observable<AuthResult> LogOn(@Body RequestBody params);

    @Headers("Content-Type: application/json")
    @POST("logout")
    Observable<NullClass> LogOut(@Body RequestBody params);
}
