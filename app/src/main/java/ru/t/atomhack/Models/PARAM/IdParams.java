package ru.t.atomhack.Models.PARAM;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class IdParams extends SessionParams implements Serializable {

    public IdParams(Integer id, String authKey) {
        super(authKey);
        Id = id;
    }

    @SerializedName("id")
    public Integer Id;

    @Override
    public String ToString() {
        return gson.toJson(this);
    }
}
