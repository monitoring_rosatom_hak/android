package ru.t.atomhack.Models;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.t.atomhack.App;
import ru.t.atomhack.Models.Api.AuthApi;
import ru.t.atomhack.Models.DTO.AuthResult;
import ru.t.atomhack.Models.PARAM.AuthParams;
import ru.t.atomhack.Models.PARAM.LogoutParams;
import rx.Observable;


public class AuthModelImpl extends BaseModelImpl implements AuthModel {

    private static String SessionId;

    @Inject
    AuthApi authApi;

    public AuthModelImpl() {
        super();
        App.getComponent().inject(this);
    }

    @Override
    public Observable<AuthResult> LogOn(AuthParams params) {
        return authApi.LogOn(RequestBody.create(MediaType.parse("text/plain"), params.ToString())).compose(applySchedulers());
    }
    @Override
    public Observable<NullClass> LogOut(LogoutParams params) {
        return authApi.LogOut(RequestBody.create(MediaType.parse("text/plain"), params.ToString())).compose(applySchedulers());
    }

    @Override
    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    @Override
    public String getSessionId() {
        return SessionId;
    }


}
