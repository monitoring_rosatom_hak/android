package ru.t.atomhack.Models.Api;

import retrofit2.Retrofit;

public final class SettingsApiModule extends BaseApiModule {

    public SettingsApiModule() {
    }

    public static SettingsApi getRestApiInterface(String url) {
        Retrofit.Builder builder = getRetroBuilder(url);
        SettingsApi settingsApi = builder.build().create(SettingsApi.class);
        return settingsApi;
    }

}
