package ru.t.atomhack.Models.Api;


import java.util.ArrayList;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;

import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.Request;
import ru.t.atomhack.Models.NullClass;
import rx.Observable;


public interface RequestApi {
    @GET("types")
    Observable<ArrayList<CodeNameItem>> GetTypes(@HeaderMap Map<String, String> headers);

    @GET("request")
    Observable<Request> GetRequest(@Query("id") int id, @HeaderMap Map<String, String> headers);

    @POST("request")
    Observable<NullClass> SaveRequest(@Body RequestBody params, @HeaderMap Map<String, String> headers);

}
