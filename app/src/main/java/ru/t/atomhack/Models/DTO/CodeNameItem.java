package ru.t.atomhack.Models.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CodeNameItem implements Serializable {
    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public CodeNameItem(String code, String name) {
        this.code = code;
        this.name = name;
    }

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;
}
