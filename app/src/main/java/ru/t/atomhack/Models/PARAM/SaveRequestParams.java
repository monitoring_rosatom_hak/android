package ru.t.atomhack.Models.PARAM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveRequestParams extends IdParams {
    public SaveRequestParams(Integer id, String reason, String description, String img, String authKey) {
        super(id, authKey);
        this.reason = reason;
        this.description = description;
        this.img = img;
    }

    @SerializedName("reason")
    private String reason;

    @SerializedName("description")
    private String description;

    @SerializedName("img")
    private String img;

    @Override
    public String ToString() {
        return gson.toJson(this);
    }
}
