package ru.t.atomhack.Models;

import java.util.ArrayList;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.t.atomhack.App;
import ru.t.atomhack.Models.Api.SettingsApi;
import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SaveSettingsParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;


public class SettingsModelImpl extends BaseModelImpl implements SettingsModel {

    @Inject
    SettingsApi settingsApi;

    public SettingsModelImpl() {
        super();
        App.getComponent().inject(this);
    }

    @Override
    public Observable<ArrayList<IdNameCheckItem>> GetSettings(CodeParams params) {
        return settingsApi.GetSettings(params.code, InitHeaders(params)).compose(applySchedulers());
    }

    @Override
    public Observable<NullClass> SaveSettings(SaveSettingsParams params) {
        return settingsApi.SaveSettings(RequestBody.create(MediaType.parse("text/plain"), params.ToString()), InitHeaders(params)).compose(applySchedulers());
    }

}
