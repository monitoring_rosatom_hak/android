package ru.t.atomhack.Models;

import java.util.ArrayList;

import javax.inject.Inject;

import ru.t.atomhack.App;
import ru.t.atomhack.Models.Api.MapApi;
import ru.t.atomhack.Models.DTO.PointItem;
import ru.t.atomhack.Models.PARAM.CodeParams;
import rx.Observable;


public class MapModelImpl extends BaseModelImpl implements MapModel {

    @Inject
    MapApi requestApi;

    public MapModelImpl() {
        super();
        App.getComponent().inject(this);
    }
    @Override
    public Observable<ArrayList<PointItem>> GetEventList(CodeParams params) {
        return requestApi.GetEventList(params.code, InitHeaders(params)).compose(applySchedulers());
    }
}
