package ru.t.atomhack.Models.DTO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PointItem extends IdNameItem {

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public Double getLatitude() {
        return latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public PointItem(Integer id, String name, String type, String description, Double latitude, Double longitude) {
        super(id, name);
        this.type = type;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;
    }



    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("latitude")
    @Expose
    private Double latitude;

    @SerializedName("longitude")
    @Expose
    private Double longitude;
}
