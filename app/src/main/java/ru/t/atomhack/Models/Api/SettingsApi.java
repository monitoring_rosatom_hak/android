package ru.t.atomhack.Models.Api;


import java.util.ArrayList;
import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Query;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameCheckItem;
import ru.t.atomhack.Models.NullClass;
import rx.Observable;


public interface SettingsApi {
    @GET("settings")
    Observable<ArrayList<IdNameCheckItem>> GetSettings(@Query("code") String code, @HeaderMap Map<String, String> headers);

    @POST("settings")
    Observable<NullClass> SaveSettings(@Body RequestBody params, @HeaderMap Map<String, String> headers);

}
