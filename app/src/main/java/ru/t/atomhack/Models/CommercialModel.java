package ru.t.atomhack.Models;

import java.util.ArrayList;

import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.IdNameDescrItem;
import ru.t.atomhack.Models.PARAM.CodeParams;
import ru.t.atomhack.Models.PARAM.SaveOrderParams;
import ru.t.atomhack.Models.PARAM.ServiceOwnerParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;

public interface CommercialModel {
    Observable<ArrayList<CodeNameItem>> GetServices(SessionParams params);
    Observable<ArrayList<CodeNameItem>> GetOwners(CodeParams params);
    Observable<ArrayList<IdNameDescrItem>> GetTariffs(ServiceOwnerParams params);
    Observable<NullClass> SaveOrder(SaveOrderParams params);
}
