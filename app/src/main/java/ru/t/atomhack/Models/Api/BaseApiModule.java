package ru.t.atomhack.Models.Api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import ru.t.atomhack.Models.Api.Converters.DateDeserializer;
import ru.t.atomhack.Models.Api.Converters.MyGsonConverterFactory;
import ru.t.atomhack.Models.ConstClass;

//@Singleton
//@Module
public class BaseApiModule {
    //Retrofit.Builder builder;
    final static private String TAG = "Tag.NetworkUtilsAcceptSelfSignedSslCert";

    protected static String token;

//    @Provides
//    @Singleton
    protected static Retrofit.Builder getRetroBuilder(String url) {
        X509TrustManager x509TrustManager = new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
                System.out.println(TAG + ": authType: " + String.valueOf(authType));
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
                System.out.println(TAG + ": authType: " + String.valueOf(authType));
            }
        };

        final TrustManager[] trustManagers = new TrustManager[]{new X509TrustManager() {
            @Override
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            @Override
            public void checkServerTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
                System.out.println(TAG + ": authType: " + String.valueOf(authType));
            }

            @Override
            public void checkClientTrusted(final X509Certificate[] chain,
                                           final String authType) throws CertificateException {
                System.out.println(TAG + ": authType: " + String.valueOf(authType));
            }
        }};

        HostnameVerifier hostnameVerifier = (hostname, session) -> {
            try {
                return (hostname.equals((new URL(ConstClass.BASE_URL)).getHost()));
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            }
        };

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        try {
            SSLContext sslContext = SSLContext.getInstance("SSL");
            SecureRandom secureRandom = new SecureRandom();
            sslContext.init(null, trustManagers, secureRandom);
            httpClient.sslSocketFactory(sslContext.getSocketFactory(), x509TrustManager);
        } catch (Exception e) {
            e.printStackTrace();
            //System.out.println(e.getMessage());
        }
        httpClient.hostnameVerifier(hostnameVerifier);
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();

        return new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(MyGsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(httpClient.build());
    }
}
