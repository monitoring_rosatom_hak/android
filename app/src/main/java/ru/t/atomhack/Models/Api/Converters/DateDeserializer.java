package ru.t.atomhack.Models.Api.Converters;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static ru.t.atomhack.Utils.GlobalUtils.ToDate;

public class DateDeserializer implements JsonDeserializer<Date>, JsonSerializer<Date> {
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException
    {
        String s = json.getAsJsonPrimitive().getAsString();
        long l;
        if (s.contains("+")) {
            l = Long.parseLong(s.substring(s.indexOf("(") + 1, s.indexOf("+")));
        }else if (s.contains("(")){
            l = Long.parseLong(s.substring(s.indexOf("(") + 1, s.indexOf(")")));
        }else{
            return ToDate(s, "yyyy-mm-dd hh24:mi:ss");
        }
        return new Date(l);
    }

    @Override
    public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext context) {
        Date res = serverTimeShift(src);
        return src == null ? null : new JsonPrimitive("/Date(" + String.valueOf(res.getTime()) + "+0)/");
    }

    private Date serverTimeShift(Date src) {
        Calendar cal = Calendar.getInstance();

        cal.setTime(src);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int min = cal.get(Calendar.MINUTE);
        int sec = cal.get(Calendar.SECOND);

        cal = Calendar.getInstance(TimeZone.getTimeZone("GMT+7:00"));
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, sec);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

}
