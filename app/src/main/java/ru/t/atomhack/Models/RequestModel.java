package ru.t.atomhack.Models;

import java.util.ArrayList;

import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.Request;
import ru.t.atomhack.Models.PARAM.IdParams;
import ru.t.atomhack.Models.PARAM.SaveRequestParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;

public interface RequestModel {
    Observable<ArrayList<CodeNameItem>> GetTypes(SessionParams params);
    Observable<Request> GetRequest(IdParams params);
    Observable<NullClass> SaveRequest(SaveRequestParams params);
}
