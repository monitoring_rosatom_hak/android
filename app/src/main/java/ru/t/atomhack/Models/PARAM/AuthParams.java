package ru.t.atomhack.Models.PARAM;

public class AuthParams extends BaseParams {

    public String username;
    public String password;

    @Override
    public String ToString() {
        String str = new com.google.gson.Gson().toJson(this);
        return str;
    }
}
