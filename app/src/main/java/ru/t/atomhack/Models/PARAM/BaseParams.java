package ru.t.atomhack.Models.PARAM;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Date;

import ru.t.atomhack.Models.Api.Converters.DateDeserializer;


public abstract class BaseParams {

    public BaseParams(){
        gson = new GsonBuilder()
                .registerTypeAdapter(Date.class, new DateDeserializer())
                .create();
    }

    protected static Gson gson;

    public abstract String ToString();
}
