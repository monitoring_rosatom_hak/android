package ru.t.atomhack.Models;

import java.util.ArrayList;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import ru.t.atomhack.App;
import ru.t.atomhack.Models.Api.RequestApi;
import ru.t.atomhack.Models.DTO.CodeNameItem;
import ru.t.atomhack.Models.DTO.Request;
import ru.t.atomhack.Models.PARAM.IdParams;
import ru.t.atomhack.Models.PARAM.SaveRequestParams;
import ru.t.atomhack.Models.PARAM.SessionParams;
import rx.Observable;


public class RequestModelImpl extends BaseModelImpl implements RequestModel {

    @Inject
    RequestApi requestApi;

    public RequestModelImpl() {
        super();
        App.getComponent().inject(this);
    }

    @Override
    public Observable<ArrayList<CodeNameItem>> GetTypes(SessionParams params) {
        return requestApi.GetTypes(InitHeaders(params)).compose(applySchedulers());
    }

    @Override
    public Observable<Request> GetRequest(IdParams params) {
        return requestApi.GetRequest(params.Id, InitHeaders(params)).compose(applySchedulers());
    }

    @Override
    public Observable<NullClass> SaveRequest(SaveRequestParams params) {
        return requestApi.SaveRequest(RequestBody.create(MediaType.parse("text/plain"), params.ToString()), InitHeaders(params)).compose(applySchedulers());
    }
}
