package ru.t.atomhack.Models;

public interface ConstClass {
    String UI_THREAD = "UI_THREAD";
    String IO_THREAD = "IO_THREAD";

    String BASE_URL = "https://virtserver.swaggerhub.com/atomhack_din/atom_hack/1.0.0/";
}
