package ru.t.atomhack.Models;

import ru.t.atomhack.Views.Fragments.BaseFragment;

public class ActionItem {
    public String getTitle() {
        return title;
    }

    public BaseFragment getBaseFragment() {
        return baseFragment;
    }

    public ActionItem(String title, BaseFragment baseFragment) {
        this.title = title;
        this.baseFragment = baseFragment;
    }

    private String title;
    private BaseFragment baseFragment;
}
