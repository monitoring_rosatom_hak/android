package ru.t.atomhack.Models.PARAM;

public class SessionParams extends BaseParams {

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public SessionParams(String authKey) {
        this.authKey = authKey;
    }

    String authKey;

    @Override
    public String ToString() {
        return null;
    }
}
