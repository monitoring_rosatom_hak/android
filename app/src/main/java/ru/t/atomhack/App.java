package ru.t.atomhack;

import android.app.Application;
import android.content.Context;

import ru.t.atomhack.DI.Components.AppComponent;
import ru.t.atomhack.DI.Components.DaggerAppComponent;
import ru.t.atomhack.DI.Modules.AppModule;


public class App extends Application {

    private static AppComponent component;
    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = buildComponent(this);
    }

    protected AppComponent buildComponent(Context context) {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

    }

}
